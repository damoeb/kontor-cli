import * as yaml from 'js-yaml';
import * as fs from 'fs';
import {find, isArray, reduce} from 'lodash';
import {Article, DirtySources, Source, Account} from './models/models';
import {Parser} from "./parser/Parser";

interface UserConfig {
}

export class ParserManager {
  constructor(private config: UserConfig) {

  }
  getParserByName(name:String):Parser {
    throw new Error('not yet implemented');
  }
}

export class Helper {

  static getPathToSources() {
    // todo use pwd instead
    return '../example/config/sources.yml';
  }

  static getPathToInbox() {
    return '../example/archive/inbox.json';
  }

  static getPathToArchive() {
    return '../example/archive/archive.json';
  }

  static getPathToAccount() {
    return '../example/config/kontor.yml';
  }

  static async getSources(): Promise<Source[]> {
    const dirtySources: DirtySources = yaml.safeLoad(fs.readFileSync(Helper.getPathToSources(), 'utf8'));

    return reduce(dirtySources, (normalizedSources: Source[], source, name) => {

      let urls: string[],
        whitelisted = false,
        tags: string[] = [],
        filterText: string;

      if (typeof (source) === 'string') {
        urls = [source];
      } else {
        urls = isArray(source.url) ? source.url : [source.url];
        whitelisted = source.whitelisted || whitelisted;
        tags = source.tags || tags;
        filterText = source.filterText;
      }

      urls.forEach(url => {
        normalizedSources.push({url, whitelisted, tags, name, filterText});
      });

      return normalizedSources;
    }, []);
  }

  static async getAccount(): Promise<Account> {
    const account: Account = yaml.safeLoad(fs.readFileSync(Helper.getPathToAccount(), 'utf8'));
    return account;
  }

  static async getInbox(): Promise<Article[]> {
    const sources = await Helper.getSources();
    const articles: Article[] = JSON.parse(fs.readFileSync(Helper.getPathToInbox(), 'utf8'));
    return articles.map(article => {
      article.createdAt = new Date(article.createdAt);
      article.source = find(sources, {url: article.source.url});
      return article;
    });
  }

  static async getArchive(): Promise<Article[]> {

    const articles: Article[] = JSON.parse(fs.readFileSync(Helper.getPathToArchive(), 'utf8'));
    return articles.map(article => {
      article.createdAt = new Date(article.createdAt);
      return article;
    });
  }

  static async getConfig(): Promise<ParserManager> {
    return new ParserManager({});
  }
}
