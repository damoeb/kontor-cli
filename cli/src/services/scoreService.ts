import {Article} from '../models/models';

export class ScoreService {
  constructor() {
    this.score = this.score.bind(this);
  }

  async score(articles: Article[]): Promise<Article[]> {
    return articles.map(article => {
      const features = article.features;
      article.computedScore = this.getComputedScore(features.points, features.linkCount, features.wordCount);
      return article;
    });
  }

  private getComputedScore(points: Promise<number>, linkCount: number, wordCount: number): number {
    // todo return Math.log(points) + linkCount + Math.log(wordCount);
    return linkCount + Math.log(wordCount);
  }
}

export default new ScoreService();
