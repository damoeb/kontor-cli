import {Article} from '../models/models';
import * as sprintf from 'sprintf-js';
import {sortBy} from 'lodash';
import chalk from 'chalk';

export class PrintService {
  constructor() {
    this.print = this.print.bind(this);
  }

  print(count: number) {
    return (articles: Article[]) => {
      sortBy(articles, article => article.source.whitelisted ? -1000 : -article.computedScore)
        .slice(0, count)
        .forEach((article, index) => {
          // const date = dayjs(article.createdAt).format('DD.MM.YY');
          // const sdate = sprintf.sprintf('%-10s', date);
          const sfeed = sprintf.sprintf('%-20s', article.source.name);
          const sindex = sprintf.sprintf('%-3s', index + 1);
          const sscore = chalk.red.bold(sprintf.sprintf('%-4s', article.source.whitelisted ? '*' : Math.round(article.computedScore)));
          console.log(`${sindex}. ${sfeed} ${sscore} ${this.substring(article.title, 100)}`);
          console.log(`${sprintf.sprintf('%-29s', '')}  ${chalk.gray(article.textContent.replace(/[\n\r\t ]+/g, ' ').trim().substring(0, 140))}`);
          console.log(`${sprintf.sprintf('%-29s', '')}  ${chalk.gray.underline.italic(article.htmlUrl)}`);
          console.log(`${sprintf.sprintf('%-29s', '')}  ${chalk.gray(article.tags.join(', '))}`);
          console.log('');
        })
    };
  }

  private substring(value: string, maxLength: number) {
    if (value.length > maxLength) {
      return value.substring(0, maxLength) + '...'
    }
    return value;
  }
}

export default new PrintService();
