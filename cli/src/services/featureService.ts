import {sum} from 'lodash';
import {Article} from "../models/models";

export class FeatureService {
  constructor() {
    this.extractFeatures = this.extractFeatures.bind(this);
  }

  async extractFeatures(articles: Article[]): Promise<Article[]> {
    /*
    FEATURES:
      content:
        complexity:
        outgoing-links-count:
        word-count:
        structure:
        language:
      consistency:
      external-activity-generation:
        incoming-links-count:
        comments:
        shares-on-social-media:

     */

    return articles.map(article => {
      // todo get medium
      // <button class="button button--chromeless u-baseColor--buttonNormal js-multirecommendCountButton u-marginLeft4" data-action="show-recommends" data-action-value="2c6301d2073d">2.8K</button>
      // todo extract disqus comment count and comments? example https://www.quantamagazine.org/been-kim-is-building-a-translator-for-artificial-intelligence-20190110/
      // todo extract hn, lobsters, dev.to comment count and comments?
      // https://hacker-news.firebaseio.com/v0/
      article.features = {
        points: Promise.all([
          this.getLinkedinShares(article),
          this.getFacebookShares(article),
          this.getHackerNewsPoints(article)
        ]).then(sum),
        linkCount: this.getLinkCount(article),
        wordCount: this.getWordCount(article)
      };
      return article;
    });
  }

  private async get(url: string): Promise<any> {
    throw new Error('not implemented');
  }

  private getFacebookShares(article: Article): Promise<number> {
    return this.get(`https://graph.facebook.com/?id=${encodeURIComponent(article.htmlUrl)}`)
      .then(json => json.share.share_count)
      .catch(error => {
        console.error('facebook graph', error);
        return 0;
      });
  }

  private getLinkedinShares(article: Article): Promise<number> {
    return this.get(`https://www.linkedin.com/countserv/count/share?format=json&url=${encodeURIComponent(article.htmlUrl)}`)
      .then(json => json.count)
      .catch(error => {
        console.error('linkedin', error);
        return 0;
      });
  }

  private getHackerNewsPoints(article: Article): Promise<number> {
    return this.get(`https://hn.algolia.com/api/v1/search?query=${encodeURIComponent(article.htmlUrl)}`)
      .then(json => json.hits[0].points)
      .catch(error => {
        console.error('hackernews', error);
        return 0;
      });
  }

  private getWordCount(article: Article) {
    return article.textContent.split(' ').length;
  }

  private getLinkCount(article: Article) {
    return article.content.split('href').length - 1;
  }
}

export default new FeatureService();
