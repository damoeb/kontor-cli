import {Article} from "../models/models";
import {sortBy} from "lodash";

export class SortService {
  constructor() {
    this.sortBy = this.sortBy.bind(this);
  }

  public sortBy(sortFn: (article: Article) => number) {
    return (articles: Article[]) => {
      return sortBy(articles, sortFn);
    }
  }
}

export default new SortService();
