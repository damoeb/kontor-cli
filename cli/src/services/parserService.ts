import {flatten, union} from 'lodash';
import xmlFeedParser from '../parser/xmlFeedParser';
import {Article, HttpStream, Source} from '../models/models';
import {Parser} from '../parser/Parser';
import nullParser from '../parser/nullParser';
import * as request from 'request';
import {Readable} from "stream";
import chalk from "chalk";
import {ParserManager} from "../helper";

export class ParserService {

  constructor(private config: ParserManager) {

    this.parse = this.parse.bind(this);
  }

  private async findParserForSource(source: Source, stream: HttpStream): Promise<Parser> {
    // build-in parsers
    const parsers: Parser[] = [
      xmlFeedParser
    ];

    let matchedParser;
    if (source.parser) {
      matchedParser = this.config.getParserByName(source.parser);
    } else {
      matchedParser = parsers.find(parser => parser.test(source, stream.headers)) || nullParser;
    }
    console.log(`${source.name} using ${chalk.italic(matchedParser.constructor.name)}`);
    return matchedParser;

  }

  parse(sources: Source[]): Promise<Article[]> {
    return Promise.all(
      sources.map(async source => {
        // todo resolve parsed feed from remote
        return this.getStream(source)
          .then(stream => {
            return this.findParserForSource(source, stream)
              .then(parser => parser.parse(source, stream));
          })
          .catch((error:Error) => {
            console.error(source.name, error);
            return <Article[]>[];
          })
          .then(articles => this.addSourceTags(source, articles));
      })
    )
      .then(articles => flatten(articles));
  }

  protected getStream(source: Source):Promise<HttpStream> {
    return new Promise((resolvePromise, rejectPromise) => {
      const req = request(source.url);

      const timeoutId = setTimeout(() => {
        rejectPromise(`Fetching ${source.url} timed out`);
      }, 30000);

      const resolve = (readable: Readable, headers:object) => {
        clearTimeout(timeoutId);
        resolvePromise({stream: readable, headers});
      };

      const reject = (error: Error) => {
        clearTimeout(timeoutId);
        rejectPromise(error);
      };

      req.on('error', (error: string) => {
        reject(new Error(`Failed to load the feed-url ${source.url} with ${error}`));
      });

      const chunks:string[] = [];
      const headers = {};
      req.on('data', (data:Buffer) => {
        chunks.push(data.toString());
      });

      req.on('end', () => {
        const readable = new Readable();
        readable._read = function () {
          this.push(chunks.join(''));
          this.push(null);
        };

        resolve(readable, headers);
      });

      req.on('response', (res:request.Response) => {
        if (res.statusCode !== 200) {
          reject(new Error(`Failed to load the feed-url ${source.url} with statusCode ${res.statusCode}`));
        }
        Object.assign(headers, res.headers);
      });
    });
  }

  private addSourceTags(source: Source, articles: Article[]): Article[] {
    return articles.map(article => {
      article.tags = union([], article.tags, source.tags);
      return article;
    });
  }
}
