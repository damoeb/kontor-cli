import * as dayjs from 'dayjs';
import {flatten, groupBy, map, take} from 'lodash';
import {Article} from '../models/models';

export class FilterService {
  constructor() {
    this.applyTimeFilters = this.applyTimeFilters.bind(this);
    this.limitFeedSize = this.limitFeedSize.bind(this);
    this.applyTextFilters = this.applyTextFilters.bind(this);
  }

  applyTimeFilters(withinDays: number) {
    return async (articles: Article[]): Promise<Article[]> => {
      const minDate: dayjs.Dayjs = dayjs().subtract(withinDays, 'day');
      return articles
        .filter(article => dayjs(article.createdAt).isAfter(minDate));
    }
  }

  async applyTextFilters(articles: Article[]): Promise<Article[]> {
    return articles
      .filter(article => !article.source.filterText || new RegExp(article.source.filterText, 'ig').test(article.content));
  }

  applyQueryFilter(query: string) {
    return async (articles: Article[]): Promise<Article[]> => {
      let needle = new RegExp(query, 'ig');
      return articles
        .filter(article => !query || needle.test(article.content) || needle.test(article.htmlUrl) || needle.test(article.author));
    }
  }


  limitFeedSize(maxPerFeed: number) {
    return async (articles: Article[]): Promise<Article[]> => {
      return flatten(map(groupBy(articles, article => article.source.url), (articlesInFeed) => {
        return take(articlesInFeed, maxPerFeed);
      }));
    };
  }
}

export default new FilterService();
