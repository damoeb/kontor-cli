import * as program from 'commander';
import * as fs from 'fs';
import {Article} from '../models/models';
import logger from '../logger';
import {ParserService} from '../services/parserService';
import {Helper} from "../helper";

/*
  pull [registry]      Fetch all articles
 */
export function pull() {program
  .command('pull [registry]')
  .description('Fetch all articles')
  .action(async (registry = 'origin', cmd) => {
    logger.debug(`pull ${registry}`);
    const articles: Article[] = await Helper.getSources()
      .then(new ParserService(await Helper.getConfig()).parse)
      .then(articles => {
        return articles.map(article => {
          article.source = {
            url: article.source.url,
            name: article.source.name
          };
          return article;
        });
      });

    fs.writeFileSync(Helper.getPathToInbox(), JSON.stringify(articles));
    console.info(`Pulled ${articles.length} articles`);
  });
}
