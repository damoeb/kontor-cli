import * as program from 'commander';
import * as yaml from 'js-yaml';
import * as fs from 'fs';
import * as request from 'request';
import {find, flatten, isString, shuffle, some, takeRight, uniq} from 'lodash';
import {Article, DirtySource, DirtySources} from './models/models';
import filterService from './services/filterService';
import scoreService from './services/scoreService';
import printService from './services/printService';
import puppeteerService from './services/puppeteerService';
import sortService from './services/sortService';
import logger from './logger';
import featureService from './services/featureService';
import {ParserService} from './services/parserService';
import chalk from "chalk";
import {Parser} from "./parser/Parser";
import {Helper} from "./helper";
import {pull} from './commands/pull';

/*

COMMANDS:
  pull [registry]      Fetch all articles
  inbox                List articles of inbox
  push [registry]      Push kontor.yml and sources.yaml to remote registry
  ui [registry]        Push kontor.yml to remote registry
  archive              List articles from archive
  archive <url> <path  Archive article
  new <path>
  follow <url>         Add a feed url to sources.yaml
  ls                   List archived articles by time

 */

pull();

program
  .command('push [registry]')
  .description('Sync local conf with registry')
  .action(async (registryName = 'origin', cmd) => {
    const account = await Helper.getAccount();
    const sources = await Helper.getSources();

    account.sources = sources;

    const registry = account.registries[registryName];
    if (!registry) {
      console.error('registry not found');
      process.exit(1);
    }
    console.log(`Pushing to ${registryName} ${registry.url}`);

    account.id = registry.id;

    request.post({
      url: `${registry.url}/users`,
      body: account,
      json: true
    }, (error, response, body) => {
      if (error) {
        console.error(error);
      }
      console.log(body);
    });

  });

/*
 todo extract excerpt (readability.js)
 todo use views on inbox to filter and score articles according to that view
*/
program.command('inbox')
  .description('List articles of inbox')
  // .option('-q, --query [query]', 'Query string')
  .option('-d, --within-days [days]', 'Time range of days from now', 7)
  .option('-f, --max-per-feed [articles]', 'Max number of articles per feed', 5)
  .option('-c, --count [articles]', 'Max number of total articles', 60)
  .action(async (cmd) => {
    console.debug(`> inbox --within-days=${cmd.withinDays} --max-per-feed=${cmd.maxPerFeed} --count ${cmd.count}\n`);

    await Helper.getInbox()
    // .then(filterService.applyQueryFilter(cmd.query))
      .then(filterService.applyTextFilters)
      .then(filterService.applyTimeFilters(cmd.withinDays))
      .then(featureService.extractFeatures)
      .then(scoreService.score)
      .then(filterService.limitFeedSize(cmd.maxPerFeed))
      .then(sortService.sortBy(article => -article.computedScore))
      .then(printService.print(cmd.count));
  });

/*
  add website to archive
  cli archive https://www.quantamagazine.org/the-double-life-of-black-holes-20190129/ -q 5 -t ui,design

  will store the the readability with tags and quality in the folder structure year/
  and dumps the site
 */
program.command('archive <url> <path>')
  .description('Archive article')
  .option('-s, --simulate', 'Dry run')
  .option('-q, --quality [quality]', 'Quality of article', 3)
  .option('-t, --tag [tag ...]', 'Tags of article', [])
  .option('-a, --author [author]', 'The author')
  // .option('-d, --dump', 'Dump full website')
  .action(async (url, path, cmd) => {
    console.debug(`> add --quality ${cmd.quality} --tag ${cmd.tag} ${url} --simulate ${!!cmd.simulate}\n`);
    const {readability, feeds, links} = await puppeteerService.analyzeSite(url, true);
    delete (<any>readability).dir;

    /*
    todo archiving an article improves the source karma
    some harvests should trigger a video download

    create an atom feed too
    An archived article could be forwarded to pocket or others
     */
    const archive = await Helper.getArchive();

    const uid = new Date().getTime().toString(26);
    // const path = `example/archive/${new Date().getFullYear()}/${new Date().getMonth()}/${uid} - ${readability.title.replace(/\//g, '_')} (${FeedParser.extractDomain(url)}).json`;
    //
    // const folders = path.split('/');
    // folders.pop();
    // let cwd = process.cwd();
    // folders.forEach(folder => {
    //   cwd = `${cwd}/${folder}`;
    //   if (!fs.existsSync(cwd)) {
    //     fs.mkdirSync(cwd);
    //   }
    // });
    //
    // fs.writeFileSync(path, JSON.stringify(readability, null, 2));

    const article:Article = {
      id: uid,
      quality: cmd.quality,
      title: readability.title,
      content: readability.content,
      textContent: readability.textContent.trim(),
      // todo rename to url
      htmlUrl: url,
      excerpt: readability.excerpt,
      tags: cmd.tag.split(',').map((tag:string) => tag.trim()).filter((tag:string) => tag.length > 0),
      domain: Parser.extractDomain(url),
      createdAt: new Date(),
      relations: uniq(links.filter(link => link.url.startsWith('http')).map(link => link.url))
    };

    if (cmd.author) {
      article.author = cmd.author;
    }

    console.log(article);

    console.log(chalk.bold('Links'));
    links.slice(0, 10).forEach(link => {
      console.log(`${link.name} -> ${link.url}`);
    });

    archive.push(article);

    // if (cmd.dump) {
    //   console.log('Saving full website');
    //   execSync(`httrack ${ url } -O ${ folders.join('/') }`);
    // }

    await Helper.getSources().then(sources => {
      if (feeds.length === 0) {
        console.log(chalk.strikethrough('No feeds exposed'));
      } else {
        const following = some(feeds, feedUrl => {
          return find(sources,{url: feedUrl});
        });
        if (!following) {
          console.log(chalk.bold('Feeds'));

          feeds.forEach(feed => {
            console.log(feed)
          });
        }
      }
    });


    if (!cmd.simulate) {
      fs.writeFileSync(Helper.getPathToArchive(), JSON.stringify(archive, null, 2));
      console.log(chalk.bold(`> ${uid}`));
    }
  });

/*
  search archive
 */
program.command('search <query>')
  .description('Filtered list of archive and inbox')
  .option('-c, --count [articles]', 'Max number of total articles', 60)
  .action(async (query, cmd) => {
    console.log(`search "${query}"`);

    await Helper.getInbox()
      .then(filterService.applyQueryFilter(query))
      .then(filterService.applyTextFilters)
      // .then(scoreService.scoreByQuery(query))
      .then(sortService.sortBy(article => -article.computedScore))
      .then(printService.print(cmd.count));

    // todo user could search archive inbox or remote for feeds
    // const searchCmd = `grep -R -i -c ${cmd.keyword} *`;
  });

program.command('view <id>')
  .description('View archived file')
  .option('-u, --url', 'Archived url')
  .action(async (id, cmd) => {
    // todo use hash (unique)
    console.log('view', id);

    const archive = await Helper.getArchive();
    console.log(find(archive, {id}));

  });

program.command('new <path>')
  .description('Create new file in archive')
  .option('-u, --url', 'Archived url')
  .action(async (path, cmd) => {
    console.log('new', path);

  });

// program.command('status')
//   .description('Status of your knowledge database')
//   .action(async (cmd) => {
//     // console.log('status');
//     const sourcesCount = await Helper.getSources().then(sources => sources.length);
//     const inboxCount = await Helper.getInbox().then(articles => articles.length);
//     const archiveCount = await Helper.getArchive().then(articles => articles.length);
//     console.log(`${sourcesCount} sources`);
//     console.log(`${inboxCount} in inbox`);
//     console.log(`${archiveCount} in archive`);
//   });

program.command('subscribe <url>')
  .description('Subscribe to url')
  .option('--as <name>')
  .option('--cause <reason>')
  // .option('--tags <tags...>')
  .option('--whitelisted')
  .action((url, cmd) => {

    const name = cmd.as || Parser.extractDomain(url);

    console.log(`subscribe ${url} --as ${name}`);
    const dirtySources: DirtySources = yaml.safeLoad(fs.readFileSync(Helper.getPathToSources(), 'utf8'));

    // get source entry
    const origSource = dirtySources[name];
    const patchedSource:DirtySource = {
      url
    };

    if (cmd.whitelisted) {
      patchedSource.whitelisted = true;
    }
    if (cmd.cause) {
      patchedSource.why = cmd.cause;
    }

    if (isString(origSource)) {
      patchedSource.url = origSource;
    } else {
      Object.assign(patchedSource, origSource);
    }


    // -- Replace

    dirtySources[name] = patchedSource;

    fs.writeFileSync(Helper.getPathToSources(), yaml.safeDump(dirtySources), 'utf8');

    console.log(patchedSource);

  });

program.command('crosslink')
  .description('crosslink')
  .action(async (cmd) => {

    // todo try serendipity
    // console.log('Crosslink');

    const archive = await Helper.getArchive();
    const urls = shuffle(flatten(takeRight(archive, 5).map(article => article.relations)))
      .slice(0, 4);

    const links = await Promise.all(urls.map(url => {
      return puppeteerService.analyzeSite(url).then(analysis => {
        return {
          title: analysis.readability.title,
          excerpt: analysis.readability.excerpt,
          url
        };
      })
    }));
    console.log(links);

  });

program.parse(process.argv);
