import * as request from 'request';
import {isNull, map, omitBy} from 'lodash';
import * as feedparser from 'feedparser';
import logger from '../logger';
import {Article, AtomFeedItem, Feed, HttpStream, Source} from '../models/models';
import {Parser} from './Parser';
import {Readable} from "stream";


function compact(obj: object) {
  return omitBy(obj, (val: any, key: any) => {
    return isNull(val) || key.indexOf(':') > -1 || key === 'meta'
  });
}

export class XmlFeedParser extends Parser {

  constructor() {
    super();
    this.parseXmlFeed = this.parseXmlFeed.bind(this);
  }

  private parseXmlFeed(stream: Readable): Promise<Feed> {
    return new Promise((resolve, reject) => {

      const feedOptions: feedparser.Options = {
        resume_saxerror: true
      };
      const feedParser = new feedparser(feedOptions);

      stream.pipe(feedParser);

      const feed: Feed = {
        items: []
      };

      feedParser.on('end', () => {
        resolve(feed);
      });

      feedParser.on('error', (error: string) => {
        reject(new Error(`Failed to parse feed data with ${error}`));
      });

      feedParser.on('readable', function () {

        // // This is where the action is!
        const stream = this; // `this` is `feedParser`, which is a stream
        // const meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
        let item;
        //
        while (item = stream.read()) {
          feed.items.push(<any>compact(item))
        }
      });
    })
  }

  async parse(source: Source, stream: HttpStream): Promise<Article[]> {

    return this.parseXmlFeed(stream.stream)
      .then((feed: Feed) => {
        return map(<any>feed.items, (item: AtomFeedItem) => {
          const article = new Article();
          article.source = source;
          // article.group = group;
          article.createdAt = item.pubdate;
          article.author = item.author;
          // article.tags = union(item.categories, feed.tags);
          article.tags = item.categories;
          article.title = item.title;
          article.htmlUrl = item.link;
          article.content = item.description || '';
          article.textContent = this.removeTags(item.description);
          article.excerpt = this.removeTags(item.summary);
          article.feedUrl = source.url;
          article.domain = Parser.extractDomain(item.link);
          article.providedBy = [article.domain];
          return article;
        }
      )
    })
    .catch((err: Error) => {
      logger.error(`Cannot fetch feed ${source.url}, cause '${err}'`);
      return [];
    });
  }

  test(source: Source, headers: request.Headers): boolean {
    return !headers['content-type']
      || headers['content-type'].indexOf('application/xml') > -1
      || headers['content-type'].indexOf('text/xml') > -1
      || headers['content-type'].indexOf('application/atom+xml') > -1
      || headers['content-type'].indexOf('application/rss+xml') > -1;
  }
}

export default new XmlFeedParser();
