import {Article, HttpStream, Source} from '../models/models';
import {Parser} from "./Parser";
import * as request from "request";

export class NullParser extends Parser {

  parse(source: Source, stream: HttpStream): Promise<Article[]> {
    return Promise.resolve([]);
  }

  test(source: Source, headers: request.Headers): boolean {
    return true;
  }
}

export default new NullParser();
