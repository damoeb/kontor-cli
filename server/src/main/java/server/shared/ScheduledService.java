package server.shared;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.ArticleScorerService;
import server.article.ArticleService;
import server.classifier.ClassifierService;
import server.feed.FeedRepository;
import server.feed.parser.FeedParser;
import server.user.UserService;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ScheduledService {

  private final Logger logger = Logger.getLogger(ScheduledService.class.getName());
  private final ArticleService articleService;
  private final FeedRepository feedRepository;
  private final ArticleScorerService articleScoreService;
  private final ParserService parserService;
  private final UserService userService;
  private final ClassifierService classifierService;
  private boolean busyWithFeedUpdates = false;
  private boolean busyWithInboxFilling = false;

  public ScheduledService(final FeedRepository feedRepository,
                          final UserService userService,
                          final ArticleService articleService,
                          final ClassifierService classifierService,
                          final ParserService parserService,
                          final ArticleScorerService articleScoreService) {
    this.feedRepository = feedRepository;
    this.classifierService = classifierService;
    this.userService = userService;
    this.articleService = articleService;
    this.parserService = parserService;
    this.articleScoreService = articleScoreService;
  }

  @Scheduled(fixedRate = 20000)
  public void fillInboxes() {
    if (!busyWithInboxFilling) {
      try {
        busyWithInboxFilling = true;
        userService.fillInboxes();
      } finally {
        busyWithInboxFilling = false;
      }
    }
  }

  @Scheduled(fixedRate = 60000)
  public void fetchFeedUpdates() {
    if (!busyWithFeedUpdates) {
      try {
        busyWithFeedUpdates = true;
        feedRepository.findAll().forEach(feed -> {

          try {
            FeedParser matchedParser = parserService.getParserByName(feed.getParser());
            List<Article> articles = parserService.parseFeed(matchedParser, feed.getUrl());
            long savedCount = articles
              .stream()
              .peek(article -> {
                article.setFeedId(feed.getId());
                article.setScore(articleScoreService.score(article));
                Set<String> tags = classifierService.extractTags(article);
                article.setTags(tags);
              })
              .map(articleService::save)
              .filter(saved -> saved)
              .count();

            if (savedCount > 0) {
              logger.info(String.format("%s returns %s articles %s new", feed.getUrl(), articles.size(), savedCount));
            }

            feed.setLastModifiedAt(new Date());
            feedRepository.save(feed);

          } catch (Exception e) {
            logger.log(Level.WARNING, String.format("Cannot process %s Reason: %s", feed.getUrl(), e.getMessage()));
          }
        });
      } finally {
        busyWithFeedUpdates = false;
      }
    }
  }
}
