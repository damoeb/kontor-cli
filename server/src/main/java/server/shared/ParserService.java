package server.shared;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.feed.parser.FeedParser;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@Service
public class ParserService {

  private final Logger logger = Logger.getLogger(ParserService.class.getName());
  private final List<FeedParser> feedParsers;
  private final DownloadService downloadService;

  public ParserService(final List<FeedParser> feedParsers,
                       final DownloadService downloadService) {
    this.downloadService = downloadService;
    this.feedParsers = feedParsers; // todo should be ordered
  }

  public FeedParser getParserByName(String parserName) {
    return feedParsers
      .stream()
//      .filter(feedParser -> feedParser.name().equals(parserName))
      .findFirst()
      .get();
  }

  public List<Article> parseFeed(FeedParser feedParser, String url) {
    return feedParser.parseArticles(downloadService.fetchUrl(url));
  }
}
