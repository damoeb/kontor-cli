package server.shared;

import org.springframework.stereotype.Service;
import server.article.Article;

import java.util.HashSet;
import java.util.Set;

@Service
public class KeywordService {

  public Set<String> extractKeywords(Article article) {
    return new HashSet<>(article.getTags());
  }
}
