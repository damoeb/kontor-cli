package server.shared;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.stream.Collectors;

@Service
public class DownloadService {

  public String fetchUrl(String url) {
    try {
      HttpClientBuilder cb = HttpClientBuilder.create();
      SSLContextBuilder sslcb = new SSLContextBuilder();
      sslcb.loadTrustMaterial(null, new TrustStrategy() {
        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType)
          throws CertificateException {
          return true;
        }
      });
//      sslcb.loadTrustMaterial(KeyStore.getInstance(KeyStore.getDefaultType()), new TrustSelfSignedStrategy());
      cb.setSSLContext(sslcb.build());
      CloseableHttpClient httpclient = cb.build();
      HttpGet request = new HttpGet(url);
      CloseableHttpResponse response = httpclient.execute(request);

      return new BufferedReader(new InputStreamReader(response.getEntity().getContent()))
        .lines().collect(Collectors.joining("\n"));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
