package server.classifier;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import server.article.Article;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ClassifierService {

  private static final Logger logger = Logger.getLogger(ClassifierService.class.getName());
  private static final Double TITLE_BOOST = 1.2;
  private final TokenizerModel tokenizerModel;
  private final POSModel pm;
  private final ChunkerModel chunkerModel;

  private TermFrequencyRepository termFrequencyRepository;

  public ClassifierService(TermFrequencyRepository termFrequencyRepository) {
    this.termFrequencyRepository = termFrequencyRepository;
    try {
      tokenizerModel = new TokenizerModel(new FileInputStream(new ClassPathResource("en-token.bin").getFile()));
      pm = new POSModel(new FileInputStream(new ClassPathResource("en-pos-maxent.bin").getFile()));
      chunkerModel = new ChunkerModel(new FileInputStream(new ClassPathResource("en-chunker.bin").getFile()));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public List<Pair<String, Double>> tfidf(String textContent) {
    if (textContent.trim().isEmpty()) {
      return Collections.emptyList();
    }
    Set<String> candidateKeywords = extractPhrases(textContent);
    Map<String, Double> termFrequency = tf(textContent);
    return candidateKeywords.stream()
      .map(phrase -> phrase.replaceAll("^\\W|\\W$", "").trim())
      .filter(filterStopWords())
      .map(phrase -> scorePhrase(termFrequency, phrase))
      .sorted((phraseA, phraseB) -> {
        if (phraseA.getRight() > phraseB.getRight()) {
          return -1;
        }
        if (phraseA.getRight() < phraseB.getRight()) {
          return 1;
        }
        return 0;
      })
      .collect(Collectors.toList());
  }

  private Map<String, Double> tf(String textContent) {
    Set<Pair<String, Integer>> absoluteTf = new HashSet<>(tokenize(textContent)).stream()
      .filter(filterStopWords())
      .map(term -> {
        int frequency = 0;
        int index = textContent.indexOf(term);
        while (index > -1) {
          frequency += 1;
          index = textContent.indexOf(term, index + term.length());
        }
        return Pair.of(term, frequency);
      })
      .collect(Collectors.toSet());

    int maxFrequency = absoluteTf
      .stream()
      .max(Comparator.comparing(Pair::getValue))
      .orElse(Pair.of("", 0))
      .getValue();

    return absoluteTf.stream()
      .map(term -> Pair.of(term.getKey(), term.getValue() / (double) Math.max(maxFrequency, 1)))
      .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
  }

  private List<String> tokenize(String textContent) {
    return Arrays.asList(textContent.split("\\W"));
  }

  private Pair<String, Double> scorePhrase(Map<String, Double> termFrequency, String phrase) {
    OptionalDouble score = tokenize(phrase)
      .stream()
      .filter(filterStopWords())
      .map(candidate -> calculateTfIdfFeature(termFrequency, candidate))
      .mapToDouble(Pair::getValue)
      .average();
    return Pair.of(phrase, score.orElse(0));
  }

  private Predicate<String> filterStopWords() {
    return token -> token.length() > 1;
  }

  private Pair<String,Double> calculateTfIdfFeature(Map<String, Double> termFrequency, String candidate) {
    Optional<TermFrequencyItem> tfi = termFrequencyRepository.findByToken(candidate);
    Double tf = termFrequency.getOrDefault(candidate, 0d);
    double idf = tfi.orElse(new TermFrequencyItem(candidate, 1d)).getIdf();
    double tfidf = idf * tf;
    return Pair.of(candidate, tfidf);
  }

  private Set<String> extractPhrases(String textContent) {
    // http://opennlp.sourceforge.net/models-1.5/
    // https://stackoverflow.com/questions/21552647/grouping-all-named-entities-in-a-document/21554286#21554286
    TokenizerME wordBreaker = new TokenizerME(tokenizerModel);
    POSTaggerME posme = new POSTaggerME(pm);
    ChunkerME chunkerME = new ChunkerME(chunkerModel);
    //this is your sentence
//      String sentence = article.getTextContent();
    //words is the tokenized sentence
    String[] words = wordBreaker.tokenize(textContent);
    //posTags are the parts of speech of every word in the sentence (The chunker needs this info of course)
    String[] posTags = posme.tag(words);
    //chunks are the start end "spans" indices to the chunks in the words array
    Span[] chunks = chunkerME.chunkAsSpans(words, posTags);
    //chunkStrings are the actual chunks
    return new HashSet<>(Arrays.asList(Span.spansToStrings(chunks, words)));
  }

  public Set<String> extractTags(Article article) {
    Set<Pair<String, Double>> phrases = this.tfidf(article.getTitle())
      .stream()
      .map(phrase -> Pair.of(phrase.getKey(), phrase.getValue() * TITLE_BOOST))
      .collect(Collectors.toSet());
    phrases.addAll(this.tfidf(article.getExcerpt()));

    return phrases
      .stream()
      .filter(phrase -> phrase.getValue() > 0.75)
      .limit(10)
      .map(Pair::getKey)
      .collect(Collectors.toSet());
  }
}
