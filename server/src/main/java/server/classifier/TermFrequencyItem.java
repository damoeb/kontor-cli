package server.classifier;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("termfreq")
public class TermFrequencyItem {
  @Indexed
  private String token;
  private double idf;

  public TermFrequencyItem() {
  }

  public TermFrequencyItem(String token, double idf) {
    this.token = token;
    this.idf = idf;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public double getIdf() {
    return idf;
  }

  public void setIdf(double idf) {
    this.idf = idf;
  }
  //  private
}
