package server.classifier;


import org.apache.commons.lang3.tuple.Pair;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class ClassifierController {

  private ClassifierService classifierService;

  public ClassifierController(ClassifierService classifierService) {
    this.classifierService = classifierService;
  }

  @GetMapping("/classify")
  public List<Pair<String, Double>> tfidf(@RequestParam("text") String textContent) {
    return classifierService.tfidf(textContent);
  }

}
