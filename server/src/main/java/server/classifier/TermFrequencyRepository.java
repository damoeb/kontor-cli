package server.classifier;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import server.inbox.InboxItem;

import java.time.LocalDateTime;
import java.util.Optional;

public interface TermFrequencyRepository extends MongoRepository<TermFrequencyItem, String> {

  Optional<TermFrequencyItem> findByToken(String term);
}
