package server.inbox;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class InboxService {

  private InboxRepository inboxRepository;
  private static final int INBOX_SIZE_IN_DAYS = 7;

  public InboxService(InboxRepository inboxRepository) {
    this.inboxRepository = inboxRepository;
  }

  public Page<InboxItem> findAll(String userId,
                                 LocalDateTime dateTime,
                                 Pageable pageable) {
    LocalDateTime minDateTime = dateTime.minus(INBOX_SIZE_IN_DAYS, ChronoUnit.DAYS);
    LocalDateTime maxDateTime = dateTime.plus(1, ChronoUnit.DAYS);
    return inboxRepository.findByUserIdAndCreatedAtIsBetween(userId, minDateTime, maxDateTime, pageable);
  }

  public InboxItem add(InboxItem inboxItem) {
    Optional<InboxItem> persistedInboxItem = inboxRepository.findByArticleIdAndUserId(inboxItem.getArticleId(), inboxItem.getUserId());
    return persistedInboxItem.orElseGet(() -> inboxRepository.save(inboxItem));
  }
}
