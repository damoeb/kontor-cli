package server.inbox;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
public class InboxController {

  private InboxService inboxService;

  public InboxController(InboxService inboxService) {
    this.inboxService = inboxService;
  }

  @GetMapping(value = "/inbox/{userId}")
  public Page<InboxItem> get(@PathVariable("userId") String id,
                             @JsonFormat(pattern = "yyyy-MM-dd")
                             @RequestParam(value = "day", required = false) Optional<LocalDateTime> day,
                             @RequestParam(value = "page", required = false) Optional<Integer> page,
                             @RequestParam(value = "size", required = false) Optional<Integer> size) {
    return inboxService.findAll(id, day.orElse(LocalDateTime.now()), PageRequest.of(
      page.orElse(0),
      size.orElse(20),
      Sort.by(Sort.Order.desc("score")))
    );
  }

}
