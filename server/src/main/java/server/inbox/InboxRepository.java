package server.inbox;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public interface InboxRepository extends MongoRepository<InboxItem, String> {

  Page<InboxItem> findByUserIdAndCreatedAtIsBetween(String userId, LocalDateTime startDate, LocalDateTime endDate, Pageable pageable);

  Optional<InboxItem> findByArticleIdAndUserId(String articleId, String userId);
}
