package server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.logging.Logger;

@Profile("dev")
@Configuration
public class DevelopmentConfiguration {

  private Logger logger = Logger.getLogger(ProductionConfiguration.class.getName());

  public DevelopmentConfiguration() {
    logger.info("dev");
  }
}
