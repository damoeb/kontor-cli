package server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.logging.Logger;

@Profile("prod")
@Configuration
public class ProductionConfiguration {
  private Logger logger = Logger.getLogger(ProductionConfiguration.class.getName());

  public ProductionConfiguration() {
    logger.info("prod");
  }
}
