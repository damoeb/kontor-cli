package server.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

@Configuration
// https://lankydanblog.com/2017/07/16/a-quick-look-into-reactive-streams-with-spring-data-and-mongodb/
//@EnableReactiveMongoRepositories(basePackageClasses = PersonRepository.class)
//public class MongoConfig extends AbstractReactiveMongoConfiguration {
public class MongoConfig extends AbstractMongoConfiguration {

  @Override
  protected String getDatabaseName() {
    return "kontor";
  }

  @Override
  public MongoClient mongoClient() {
    return new MongoClient();
  }
}
