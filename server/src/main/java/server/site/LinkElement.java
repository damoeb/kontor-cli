package server.site;

public class LinkElement {

  private final String name;
  private final String url;
  private final String type;

  public LinkElement(String name, String url, String type) {
    this.name = name.replaceAll("[\n\r\t ]]","");
    this.url = url;
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getUrl() {
    return url;
  }
}
