package server.site;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SiteController {

  private SiteService siteService;

  public SiteController(SiteService siteService) {
    this.siteService = siteService;
  }

  @GetMapping("/site/analysis")
  public SiteAnalysis feeds(@RequestParam(name = "url") String url) {
    return siteService.analyzeSite(url);
  }

  @GetMapping("/site/favicon")
  public String favicon(@RequestParam(name = "url") String url) {
    // todo save favicon
    return String.format("https://s2.googleusercontent.com/s2/favicons?domain_url=%s", url);
  }

}
