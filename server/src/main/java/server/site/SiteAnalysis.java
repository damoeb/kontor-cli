package server.site;

import org.jsoup.nodes.Document;
import server.feed.Feed;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SiteAnalysis {
  private final Logger logger = Logger.getLogger(SiteAnalysis.class.getName());
  private final String url;

  private Set<LinkElement> feeds;
  private String title;

  public SiteAnalysis(String baseUrl, Document document) {

    List<String> types = Arrays.asList("application/atom+xml", "application/rss+xml", "application/json");
    this.feeds = document.head().childNodes().stream().filter(node -> {
      return node.nodeName().equalsIgnoreCase("link")
        && node.hasAttr("type")
        && node.hasAttr("href")
        && types.contains(node.attr("type"));
    }).map(node -> {
      return new LinkElement(node.attr("title"), absoluteUrl(baseUrl, node.attr("href")), node.attr("type"));
    }).collect(Collectors.toSet());

    this.title = document.title();
    this.url = baseUrl;
  }

  public SiteAnalysis(String url, Feed feed) {
    this.title = feed.getName();
    this.url = url;
    this.feeds = new HashSet<>(Collections.singletonList(new LinkElement(feed.getName(), feed.getUrl(), "atom")));
  }

  private String absoluteUrl(String baseUrl, String url) {
    try {
      return new URL(new URL(baseUrl), url).toString();
    } catch (MalformedURLException e) {
      logger.warning(e.getMessage());
      return url;
    }
  }

  public Set<LinkElement> getFeeds() {
    return feeds;
  }

  public String getTitle() {
    return title;
  }

  public String getUrl() {
    return url;
  }
}
