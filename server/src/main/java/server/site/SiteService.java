package server.site;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import server.feed.Feed;
import server.feed.parser.AtomFeedParser;
import server.shared.DownloadService;

import java.util.logging.Logger;

@Service
public class SiteService {

  private final Logger logger = Logger.getLogger(SiteService.class.getName());
  private final DownloadService downloadService;
  private final AtomFeedParser atomFeedParser;

  public SiteService(final DownloadService downloadService,
                     final AtomFeedParser atomFeedParser) {
    this.downloadService = downloadService;
    this.atomFeedParser = atomFeedParser;
  }

  public SiteAnalysis analyzeSite(String url) {
    String siteContent = downloadService.fetchUrl(url);

    if (isXML(siteContent)) {
      Feed feed = atomFeedParser.parseFeed(siteContent);
      feed.setUrl(url);
      return new SiteAnalysis(url, feed);
    } else {
      Document document = Jsoup.parse(siteContent);

      return new SiteAnalysis(url, document);
    }
  }

  private boolean isXML(String siteContent) {
    return siteContent.trim().startsWith("<?xml ");
  }
}
