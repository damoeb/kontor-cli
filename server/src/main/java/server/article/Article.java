package server.article;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import server.article.score.MetricResult;
import server.article.score.criterion.Comment;

import java.util.*;

public class Article {
  @Id
  private String id;
  @Indexed(unique = true)
  private String url;
  private String title;
  private String author;
  private String domain;
  private String excerpt;
  private String content;
  private String textContent;
  private String feedId;
  private double score;
  private Set<String> tags;
  private Date publishedDate;
  @CreatedDate
  private Date createdAt;
  @LastModifiedDate
  private Date lastModifiedAt;
  private Set<MetricResult> metrics = new HashSet<MetricResult>(10);
  private Locale locale = Locale.GERMAN;
  private List<Comment> comments;
  private Set<Pair<String, String>> enclosures;
//  private List<Asset> comments;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTextContent() {
    return textContent;
  }

  public void setTextContent(String textContent) {
    this.textContent = textContent;
  }

  public String getFeedId() {
    return feedId;
  }

  public void setFeedId(String feedId) {
    this.feedId = feedId;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getLastModifiedAt() {
    return lastModifiedAt;
  }

  public void setLastModifiedAt(Date lastModifiedAt) {
    this.lastModifiedAt = lastModifiedAt;
  }

  public String getExcerpt() {
    return excerpt;
  }

  public void setExcerpt(String excerpt) {
    this.excerpt = excerpt;
  }

  public Date getPublishedDate() {
    return publishedDate;
  }

  public void setPublishedDate(Date publishedDate) {
    this.publishedDate = publishedDate;
  }

  public double getScore() {
    return score;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public List<Comment> getComments() {
    return comments;
  }

  public void setComments(List<Comment> comments) {
    this.comments = comments;
  }

  public void setEnclosures(Set<Pair<String, String>> enclosures) {
    this.enclosures = enclosures;
  }

  public Set<Pair<String, String>> getEnclosures() {
    return enclosures;
  }
}
