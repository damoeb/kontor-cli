package server.article;


import com.mongodb.Cursor;
import com.mongodb.DBCursor;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ArticleRepository extends MongoRepository<Article, String> {

  boolean existsByUrl(String url);

  List<Article> findAllByFeedIdAndCreatedAtBetween(String feedId, LocalDateTime startDate, LocalDateTime endDate);
}
