package server.article.score;

@SuppressWarnings("serial")
public class ConstraintViolation extends Exception {

  public ConstraintViolation(final String message) {
    super(message);
  }

}
