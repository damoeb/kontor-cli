package server.article.score;


import server.article.Article;

public interface Constraint {

  void validate(Article toEvaluate) throws ConstraintViolation;

}
