package server.article.score.criterion.simple;


import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Wortschatz analyse
 */
@Service
public class ThesaurusCriterion extends AbstractSimpleCriterion {

  private static final Logger LOGGER = Logger.getLogger(ThesaurusCriterion.class.getName());

  private static final GermanStemmer stemmer = new GermanStemmer();

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    // -- ----------------------------------------------------------------------------------------------------------

    double ttr = getTypeTokenRatio(source);

    double performance;
    if (Goal.MODEST_TEXT == getGoal()) {
      performance = 1 - ttr;
    } else {
      performance = ttr;
    }

    return new SinglePerformance(this, performance);
  }

  /**
   * Wortschatz
   * http://de.wikipedia.org/wiki/Type-Token-Relation
   * Hoch, wenn ein großer Wortschatz verwendet wird
   */
  private double getTypeTokenRatio(Article source) {

    final Map<String, Set<String>> stemmedMap = new HashMap<String, Set<String>>(2000);

    for (Sentence s : new Content(source.getTextContent()).getSentences()) {
      for (Word w : s.getWords()) {
        try {
          String raw = w.getValue();
          if (StringUtils.isBlank(raw)) {
            continue;
          }
          String stemmed = stemmer.stem(raw);

          if (stemmed.length() == 0) continue;
          if (!stemmedMap.containsKey(stemmed)) {
            stemmedMap.put(stemmed, new HashSet<String>(3));
          }
          stemmedMap.get(stemmed).add(raw);
        } catch (Exception e) {
          LOGGER.severe("Cannot stem '" + w.getValue() + "': " + e.getMessage());
        }
      }
    }

    int typeCount = stemmedMap.size();
    int tokenCount = 0;
    for (String w : stemmedMap.keySet()) {
      tokenCount += stemmedMap.get(w).size();
    }

    return typeCount / (double) Math.max(1, tokenCount);
  }

  @Override
  public String name() {
    return "ThesaurusCriterion";
  }
}
