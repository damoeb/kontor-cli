package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.InverseOutgoingLinksCriterion;

import javax.annotation.PostConstruct;

@Service
public class Novelty extends AbstractComplexCriterion {

  private final InverseOutgoingLinksCriterion linksCriterion;

  @Autowired
  public Novelty(InverseOutgoingLinksCriterion linksCriterion) {
    this.linksCriterion = linksCriterion;
  }

  @PostConstruct
  public void onInit() {
    addCriterion(linksCriterion);
  }

  @Override
  public String name() {
    return "Novelty";
  }

}
