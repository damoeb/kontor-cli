package server.article.score.criterion.simple;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.MetricName;
import server.article.score.criterion.*;

import java.util.Locale;
import java.util.logging.Logger;

@Service
public class ArticleReadingEaseCriterion extends AbstractReadingEaseCriterion {

  private static final transient Logger LOGGER = Logger.getLogger(ArticleReadingEaseCriterion.class.getName());

  public ArticleReadingEaseCriterion() {
    //
  }


  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getContent() == null) {
      return null;
    }
    // -- ----------------------------------------------------------------------------------------------------------

    if (StringUtils.isBlank(source.getContent())) {
      return new SinglePerformance(this, 0d);
    }

    MultiplePerformance result = new MultiplePerformance(this);

    // deutsch
    if (Locale.GERMAN.equals(source.getLocale())) {
      final double amdahl = getAmdahlIndex(source.getLocale(), new Content(source.getTextContent()));
      addMetricResult(result, MetricName.AMDAHL, amdahl, Constants.AMDAHL_MIN_READABILITY, Constants.AMDAHL_MAX_READABILITY);
    }
    // flesch ist für englisch
    if (Locale.ENGLISH.equals(source.getLocale())) {
      final double flesch = getFleschIndex(source.getLocale(), new Content(source.getTextContent()));
      addMetricResult(result, MetricName.FLESCH, flesch, Constants.FLESCH_MIN_READYBILITY, Constants.FLESCH_MAX_READABILITY);
    }

    final double smog = getSMOGIndex(source.getLocale(), new Content(source.getTextContent()));
    addMetricResult(result, MetricName.SMOG, smog, Constants.SMOG_MIN_READABILITY, Constants.SMOG_MAX_READABILITY);

    return result;

  }

  @Override
  public String name() {
    return "ArticleReadingEaseCriterion";
  }

}
