package server.article.score.criterion;


import server.article.score.VectorUtils;

public abstract class AbstractSimpleCriterion implements Criterion {

  private Goal goal;

  public Goal getGoal() {
    return goal;
  }

  public void setGoal(Goal goal) {
    this.goal = goal;
  }

  public String toString() {
    return name();
  }

  protected Double getSimilarity(final Double[] a, final Double b[]) {
    return VectorUtils.distance(a, b);
  }
}
