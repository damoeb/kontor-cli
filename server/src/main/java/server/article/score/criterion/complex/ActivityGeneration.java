package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.CommentCountCriterion;
import server.article.score.criterion.simple.CommentReadingEaseCriterion;

import javax.annotation.PostConstruct;

@Service
public class ActivityGeneration extends AbstractComplexCriterion {

  private final CommentCountCriterion commentCountCriterion;
  private final CommentReadingEaseCriterion readingEaseCriterion;

  @Autowired
  public ActivityGeneration(CommentCountCriterion commentCountCriterion, CommentReadingEaseCriterion readingEaseCriterion) {
    this.commentCountCriterion = commentCountCriterion;
    this.readingEaseCriterion = readingEaseCriterion;
  }

  @PostConstruct
  public void onInit() {
    addCriterion(commentCountCriterion);
    addCriterion(readingEaseCriterion);
  }

  @Override
  public String name() {
    return "ActivityGeneration";
  }
}
