package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.Comment;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Service
@Deprecated
public class CommentsAverageFreshnessCriterion extends AbstractFreshnessCriterion {

  private static final Logger LOGGER = Logger.getLogger(CommentsAverageFreshnessCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }

    if (source.getComments() == null) {
      return null;
    }

    List<Date> dates = extractDates(source);
    if (dates.isEmpty()) {
      return null;

    } else {

      Double freshness = getFreshness(findAverageOfDates(dates));
      LOGGER.fine("eval " + name() + ": " + freshness);
      return new SinglePerformance(this, freshness);
    }
  }

  protected Date findAverageOfDates(final List<Date> dates) {

    final Calendar expiryCalendar = Calendar.getInstance();
    expiryCalendar.roll(Calendar.MONTH, -2);


    final long expiry = expiryCalendar.getTimeInMillis();

    long sum = 0l;
    for (Date date : dates) {
      sum += (date.getTime() - expiry);
    }

    return new Date(expiry + sum / dates.size());
  }

  private List<Date> extractDates(Article source) {

    final List<Date> dates = new LinkedList<Date>();
    for (Comment comment : source.getComments()) {
      dates.add(comment.publishedDate());
    }
    return dates;
  }

  @Override
  public String name() {
    return "CommentsAverageFreshnessCriterion";
  }

}
