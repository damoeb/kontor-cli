package server.article.score.criterion.simple;


import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.logging.Logger;

@Service
public class ArticleFreshnessCriterion extends AbstractFreshnessCriterion {

  private static final Logger LOGGER = Logger.getLogger(ArticleFreshnessCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getPublishedDate() == null) {
      return null;
    }
    // -- ----------------------------------------------------------------------------------------------------------

    double freshness = getFreshness(source.getPublishedDate());
    return new SinglePerformance(this, freshness);
  }

  @Override
  public String name() {
    return "ArticleFreshnessCriterion";
  }

}
