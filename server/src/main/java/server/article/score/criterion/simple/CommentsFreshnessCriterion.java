package server.article.score.criterion.simple;


import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.Comment;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.*;
import java.util.logging.Logger;

@Service
public class CommentsFreshnessCriterion extends AbstractFreshnessCriterion {

  private static final Logger LOGGER = Logger.getLogger(CommentCountCriterion.class.getName());

  private static final int HISTOGRAM_COLUMNS = 10;

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getComments() == null) {
      return null;
    }
    // -- ----------------------------------------------------------------------------------------------------------

    double result = getFreshness(findGravityCenterOfDates(extractDates(source)));

    LOGGER.fine("eval " + name() + ": " + result);
    return new SinglePerformance(this, result);
  }

  protected Date findGravityCenterOfDates(final List<Calendar> dates) {
    // histogram
    final Calendar expiryCalendar = Calendar.getInstance();
    expiryCalendar.roll(Calendar.MONTH, -2);

    final long expiry = expiryCalendar.getTimeInMillis();
    final long maxDiff = new Date().getTime() - expiry;

    final Map<Integer, Calendar> histogramLimits = getHistogramLimits(HISTOGRAM_COLUMNS, expiry, maxDiff / HISTOGRAM_COLUMNS);
    final Map<Integer, Integer> histogramFreq = initHistogramFreq(HISTOGRAM_COLUMNS);

    for (final Calendar date : dates) {

      for (int index = 0; index < HISTOGRAM_COLUMNS; index++) {
        if (date.before(histogramLimits.get(index))) {
          histogramFreq.put(index, histogramFreq.get(index) + 1);
          break;
        }

      }
    }

    // weight, with x^2

    // find max
    int maxFreq = 0;
    int dominantIndex = 0;
    for (int i = 0; i < HISTOGRAM_COLUMNS; i++) {
      if (maxFreq <= histogramFreq.get(i)) {
        maxFreq = histogramFreq.get(i);
        dominantIndex = i;
      }
    }

    return histogramLimits.get(dominantIndex).getTime();
  }

  private Map<Integer, Integer> initHistogramFreq(int columns) {
    Map<Integer, Integer> freq = new HashMap<Integer, Integer>(columns);
    for (int i = 0; i < columns; i++) {
      freq.put(i, 0);
    }
    return freq;
  }

  private Map<Integer, Calendar> getHistogramLimits(int columns, long expiryMillis, double stepMillis) {
    Map<Integer, Calendar> histogram = new HashMap<Integer, Calendar>(columns);

    for (int i = 0; i < columns; i++) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTimeInMillis(expiryMillis + (long) stepMillis * (i + 1));
      histogram.put(i, calendar);
    }

    return histogram;
  }

  private List<Calendar> extractDates(Article source) {

    final List<Calendar> dates = new LinkedList<Calendar>();
    for (Comment comment : source.getComments()) {
      Calendar date = Calendar.getInstance();
      date.setTime(comment.publishedDate());
      dates.add(date);
    }
    return dates;
  }

  @Override
  public String name() {
    return "CommentsFreshnessCriterion";
  }

}
