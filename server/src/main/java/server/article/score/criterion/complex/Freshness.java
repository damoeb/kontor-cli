package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.ArticleFreshnessCriterion;
import server.article.score.criterion.simple.CommentsFreshnessCriterion;

import javax.annotation.PostConstruct;

@Service
public class Freshness extends AbstractComplexCriterion {

  private final ArticleFreshnessCriterion articleFreshnessCriterion;
  private final CommentsFreshnessCriterion commentsfreshnessCriterion;

  @Autowired
  public Freshness(ArticleFreshnessCriterion articleFreshnessCriterion, CommentsFreshnessCriterion commentsfreshnessCriterion) {
    //
    this.articleFreshnessCriterion = articleFreshnessCriterion;
    this.commentsfreshnessCriterion = commentsfreshnessCriterion;
  }

  @PostConstruct
  public void onInit() {

    addCriterion(articleFreshnessCriterion);
    addCriterion(commentsfreshnessCriterion);

  }

  public String name() {
    return "Freshness";
  }

}
