package server.article.score.criterion;

public enum Goal {
  CHALLENGING_TEXT,
  MODEST_TEXT
}
