package server.article.score.criterion;

public interface Performance {

  Double getResult();

  Criterion getCriterion();
}
