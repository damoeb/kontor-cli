package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.AbstractSimpleCriterion;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;

import java.util.logging.Logger;

@Service
public class ViewCountCriterion extends AbstractSimpleCriterion {

  private static final transient Logger LOGGER = Logger.getLogger(ViewCountCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }

    // todo implement
//        Double views = source.getMetricResult(MetricName.VIEW_COUNT);

    return null;
  }


//        if(article.getViews()!=null) {
//            double views = getViewCount(article);
//            article.addMetricResult(MetricName.VIEW_COUNT, views);
//            LOGGER.fine("eval " + MetricName.VIEW_COUNT + ": " + views);
//        }

//    private double getViewCount(Article article) {
//        return article.getViews();
//    }

  @Override
  public String name() {
    return "ViewCountCriterion";
  }

}
