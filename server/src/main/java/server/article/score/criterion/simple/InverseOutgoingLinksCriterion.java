package server.article.score.criterion.simple;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.AbstractSimpleCriterion;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class InverseOutgoingLinksCriterion extends AbstractSimpleCriterion {

  private static final transient Logger LOGGER = Logger.getLogger(InverseOutgoingLinksCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getContent() == null) {
      return null;
    }
    // -- ----------------------------------------------------------------------------------------------------------

    Double linkCount;
    try {
      linkCount = countLinks(groupByDomain(findLinks(source)));
    } catch (Throwable t) {
      linkCount = 0d;
    }

    double result;
    if (linkCount > 0) {
      result = 1d / Math.sqrt(linkCount);
    } else {
      result = 0d;
    }
    LOGGER.fine("eval " + name() + ": " + result);
    return new SinglePerformance(this, result);
  }

  private Map<String, Integer> groupByDomain(List<String> links) {
    Map<String, Integer> groupCountMap = new HashMap<String, Integer>(links.size());

    // todo work with url object, use getHost e.g.
    for (String link : links) {

      int endIndex = link.indexOf('/', 8);
      final String domain = link.substring(0, endIndex > 0 ? endIndex : link.length());

      if (groupCountMap.containsKey(domain)) {
        groupCountMap.put(domain, groupCountMap.get(domain) + 1);
      } else {
        groupCountMap.put(domain, 1);
      }
    }

    return groupCountMap;
  }

  private Double countLinks(Map<String, Integer> links) {
    Double linkCount = 0d;

    for (String link : links.keySet()) {
      linkCount += 1d / links.get(link);
    }

    return linkCount;
  }

  private List<String> findLinks(Article source) {


    if (source.getContent() == null) {
      throw new IllegalArgumentException("no content available");
    }

    Document soup = Jsoup.parse(source.getContent());
    if (soup == null) {
      throw new IllegalArgumentException("no markup in content available");
    }

    List<String> links = new LinkedList<String>();
    for (Element a : soup.select("a[href]")) {
      links.add(a.attr("href"));
    }
    return links;
  }

  @Override
  public String name() {
    return "InverseOutgoingLinksCriterion";
  }
}
