package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.AbstractSimpleCriterion;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class IncomingLinksCriterion extends AbstractSimpleCriterion {

  private static final transient Logger LOGGER = Logger.getLogger(IncomingLinksCriterion.class.getName());

  private final transient Map<String, Integer> tweetCount = new HashMap<String, Integer>(300);

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getUrl() == null) {
      return null;
    }
//        if(!source.hasMetricResult(MetricName.TWEET_COUNT)) {
//            LOGGER.warning("Too few metric results: "+MetricName.TWEET_COUNT);
//            return null;
//        }
    // -- ----------------------------------------------------------------------------------------------------------

    final int tweetCount = getTweetCount(source);

    double result;

    if (tweetCount > 60) {
      result = 1d;

    } else {
      result = tweetCount / 60d;
    }

    LOGGER.fine("eval " + name() + ": " + result);
    return new SinglePerformance(this, result);
  }

  private int getTweetCount(final Article source) {
//    final String url = source.getUrl();
//
//    if (tweetCount.containsKey(url)) {
//      return tweetCount.get(url);
//    }
//
//    Scanner scanner = null;
//    try {
//      Thread.sleep(400); // SERVICE_TWITTER_REQ_DELAY_MSEC
//      scanner = new Scanner(new URL("http://urls.api.twitter.com/1/urls/count.json?url=" + url).openStream());
//      final StringBuilder buffer = new StringBuilder(400);
//      while (scanner.hasNext()) {
//        buffer.append(scanner.next());
//      }
//      final JSONObject jdata = new JSONObject(buffer.toString());
//      final int count = Integer.parseInt(String.valueOf(jdata.get("count")));
//      tweetCount.put(url, count);
//
//      return count;
//    } catch (Exception e) {
//      LOGGER.severe(String.format("evaluatePerformance uri %s. message: %s", source.getUrl(), e.getMessage()));
////            LOGGER.debug(e);
//    } finally {
//      if (scanner != null) {
//        scanner.close();
//      }
//    }
    return 0;
  }

  @Override
  public String name() {
    return "IncomingLinksCriterion";
  }

}
