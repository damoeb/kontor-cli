package server.article.score.criterion.complex;


import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;

@Service
public class Upcoming extends AbstractComplexCriterion {

  public Upcoming() {
    // todo survey attraction generation over time
  }

  @Override
  public String name() {
    return "Upcoming";
  }

}
