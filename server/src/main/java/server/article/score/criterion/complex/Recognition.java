package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.IncomingLinksCriterion;
import server.article.score.criterion.simple.ViewCountCriterion;

import javax.annotation.PostConstruct;

@Service
public class Recognition extends AbstractComplexCriterion {

  private final IncomingLinksCriterion incomingLinksCriterion;
  private final ViewCountCriterion viewCountCriterion;

  @Autowired
  public Recognition(IncomingLinksCriterion incomingLinksCriterion, ViewCountCriterion viewCountCriterion) {
    //
    this.incomingLinksCriterion = incomingLinksCriterion;
    this.viewCountCriterion = viewCountCriterion;
  }

  @PostConstruct
  public void onInit() {
    addCriterion(incomingLinksCriterion);
    addCriterion(viewCountCriterion);
  }

  @Override
  public String name() {
    return "Recognition";
  }

}
