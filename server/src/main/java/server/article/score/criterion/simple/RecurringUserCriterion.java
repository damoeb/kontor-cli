package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.MetricName;
import server.article.score.criterion.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RecurringUserCriterion extends AbstractSimpleCriterion {

  private static final Logger LOGGER = Logger.getLogger(RecurringUserCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    List<Comment> comments;

    // -- ASSERTIONS -- --------------------------------------------------------------------------------------------
    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }

    comments = source.getComments();
    if (comments == null) {
      return null;
    }
    // -- ----------------------------------------------------------------------------------------------------------

    if (comments.isEmpty()) {
      return new SinglePerformance(this, 0d);
    }

    final int commentCount = comments.size();

    int uniqueUserCount = getUniqueUserCount(source.getComments());
    double recurringFactor = uniqueUserCount == commentCount ? 0 : (commentCount - uniqueUserCount) / ((double) commentCount);
    LOGGER.fine("eval " + MetricName.RECURRING_FACTOR + ": " + recurringFactor);

    // 10% mentions is max
    int mentionedUserCount = getUserMentionsCount(source.getComments());
//        LOGGER.fine("eval " + MetricName.MENTIONED_USER_COUNT + ": " + mentionedUserCount);
    double mentionsFactor = mentionedUserCount > (commentCount / 10d) ? 1 : mentionedUserCount / (commentCount / 10d);

    return new SinglePerformance(this, (recurringFactor + mentionsFactor) / 2d);
  }

  private int getUserMentionsCount(List<Comment> comments) {
    if (comments == null) {
      return 0;
    }

    int mentions = 0;

    final Pattern mention = Pattern.compile("@[^ ]{4,}");

    for (Comment comment : comments) {
      Matcher matcher = mention.matcher(comment.getText());
      while (matcher.find()) {
        mentions++;
      }
    }

    return mentions;
  }

  private int getUniqueUserCount(final List<Comment> comments) {
    if (comments == null) {
      return 0;
    }
    final Set<String> users = new HashSet<String>(comments.size());
    for (Comment comment : comments) {
      users.add(comment.author());
    }
    return users.size();
  }

  @Override
  public String name() {
    return "RecurringUserCriterion";
  }

}
