package server.article.score.criterion;


import server.article.Article;

public interface Criterion {

  Performance evaluatePerformance(Article source, Goal goal);

  String name();

}
