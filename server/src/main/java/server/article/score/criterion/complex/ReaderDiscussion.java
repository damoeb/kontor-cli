package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.CommentCountCriterion;
import server.article.score.criterion.simple.CommentReadingEaseCriterion;
import server.article.score.criterion.simple.RecurringUserCriterion;

import javax.annotation.PostConstruct;

@Service
public class ReaderDiscussion extends AbstractComplexCriterion {

  private final CommentCountCriterion commentCountCriterion;
  private final RecurringUserCriterion recurringUserCriterion;
  private final CommentReadingEaseCriterion commentReadingEaseCriterion;

  @Autowired
  public ReaderDiscussion(CommentCountCriterion commentCountCriterion, RecurringUserCriterion recurringUserCriterion, CommentReadingEaseCriterion commentReadingEaseCriterion) {
    this.commentCountCriterion = commentCountCriterion;
    this.recurringUserCriterion = recurringUserCriterion;
    this.commentReadingEaseCriterion = commentReadingEaseCriterion;
  }

  @PostConstruct
  public void onInit() {
    addCriterion(commentCountCriterion);
    addCriterion(recurringUserCriterion);
    addCriterion(commentReadingEaseCriterion);
  }

  @Override
  public String name() {
    return "ReaderDiscussion";
  }

}
