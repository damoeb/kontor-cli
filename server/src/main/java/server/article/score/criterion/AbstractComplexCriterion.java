package server.article.score.criterion;


import server.article.Article;

import java.util.List;
import java.util.logging.Logger;

public abstract class AbstractComplexCriterion extends CriterionComposite<Criterion> implements Criterion {

  private static final transient Logger LOGGER = Logger.getLogger(AbstractComplexCriterion.class.getName());

  public abstract String name();

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {
    CompositePerformance performance = new CompositePerformance(this);

    for (Criterion criterion : getCriteria()) {
      Performance result = criterion.evaluatePerformance(source, goal);
      if (result == null) {
        LOGGER.fine("No performance for " + criterion.name());
      } else {
        performance.addResult(result);
      }
    }

    return performance;
  }

  public String toString() {
    StringBuilder buffer = new StringBuilder(150);
    List<Criterion> criteria = getCriteria();
    for (int i = 0; i < criteria.size(); i++) {
      buffer.append(criteria.get(i).toString());
      Double weight = getWeights().get(i);
      if (weight != 1) {
        buffer.append("^").append(weight);
      }

      if (i + 1 < criteria.size()) {
        buffer.append(", ");
      }
    }

    return name() + " {" + buffer.toString() + "}";
  }
}
