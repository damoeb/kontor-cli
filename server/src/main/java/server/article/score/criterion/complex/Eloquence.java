package server.article.score.criterion.complex;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.criterion.AbstractComplexCriterion;
import server.article.score.criterion.simple.ArticleReadingEaseCriterion;
import server.article.score.criterion.simple.ThesaurusCriterion;

import javax.annotation.PostConstruct;


@Service
public class Eloquence extends AbstractComplexCriterion {


  private final ThesaurusCriterion thesaurusCriterion;
  private final ArticleReadingEaseCriterion readingEaseCriterion;

  @Autowired
  public Eloquence(ThesaurusCriterion thesaurusCriterion, ArticleReadingEaseCriterion readingEaseCriterion) {
    this.thesaurusCriterion = thesaurusCriterion;
    this.readingEaseCriterion = readingEaseCriterion;
  }

//    public Eloquence(Goal goal) {
//        // wortschatz
//        addCriterion(new ThesaurusCriterion(goal), 1d);
//        // einfachheit
//        addCriterion(new ArticleReadingEaseCriterion(goal), 2d);
//    }

  @PostConstruct
  public void onInit() {
    // wortschatz
    addCriterion(thesaurusCriterion, 1d);
    // einfachheit
    addCriterion(readingEaseCriterion, 2d);

  }


  @Override
  public String name() {
    return "Eloquence";
  }
}
