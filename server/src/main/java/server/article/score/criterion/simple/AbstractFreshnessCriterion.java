package server.article.score.criterion.simple;

import server.article.score.criterion.AbstractSimpleCriterion;

import java.util.Calendar;
import java.util.Date;

public abstract class AbstractFreshnessCriterion extends AbstractSimpleCriterion {

  protected Double getFreshness(final Date date) {

    final Calendar expiry = Calendar.getInstance();
    expiry.roll(Calendar.MONTH, -2);

//    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
//    System.out.println("expiry "+format.format(expiry.getTime()));
//    System.out.println("date "+format.format(date.getTime()));
//    System.out.println("now "+format.format(new Date()));

    final long now = new Date().getTime();
    final double diff = now - date.getTime();
//    System.out.println("diff "+format.format(new Date((long)diff)));
    final double maxDiff = now - expiry.getTimeInMillis();
//    System.out.println("maxDiff "+format.format(maxDiff));
    double result = 0d;
    if (diff < maxDiff) {
      result = 1 - diff / maxDiff;
    }
    return result;
  }

}
