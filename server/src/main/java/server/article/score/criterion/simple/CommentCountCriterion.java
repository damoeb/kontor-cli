package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.criterion.Criterion;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.SinglePerformance;

import java.util.logging.Logger;

@Service
public class CommentCountCriterion implements Criterion {

  private static final Logger LOGGER = Logger.getLogger(CommentCountCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    if (source.getComments() == null) {
      return null;
    }

    int count = source.getComments().size();

    if (count == 0) {
      LOGGER.fine("eval " + name() + ": 0");
      return new SinglePerformance(this, 0d);
    }

    if (count == 1) {
      LOGGER.fine("eval " + name() + ": 0.5");
      return new SinglePerformance(this, 0.5d);
    }

    int maxCommentCount = 400; // LIMIT_MAX_COMMENT_COUNT
    if (count >= maxCommentCount) {
      count = maxCommentCount;
    }

    final double result = Math.log(count) / Math.log(maxCommentCount);

    LOGGER.fine("eval " + name() + ": " + result);
    return new SinglePerformance(this, result);
  }

  @Override
  public String name() {
    return "CommentCountCriterion";
  }

}
