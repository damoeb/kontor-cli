package server.article.score.criterion.simple;

import org.springframework.stereotype.Service;
import server.article.Article;
import server.article.score.MetricName;
import server.article.score.criterion.*;

import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;


@Service
public class CommentReadingEaseCriterion extends AbstractReadingEaseCriterion {

  private static final transient Logger LOGGER = Logger.getLogger(CommentReadingEaseCriterion.class.getName());

  @Override
  public Performance evaluatePerformance(final Article source, final Goal goal) {

    if (source == null) {
      throw new IllegalArgumentException("source is null");
    }
    final List<Comment> comments = source.getComments();

    if (comments == null) {
      return null;
    }
    if (comments.isEmpty()) {
      return new SinglePerformance(this, 0d);
    }

    MultiplePerformance result = new MultiplePerformance(this);

    final double amdahl = getAmdahlIndex(source.getLocale(), source.getComments());
    LOGGER.fine("eval " + MetricName.AMDAHL + ": " + amdahl);
    result.addResult(MetricName.AMDAHL, 1d, amdahl);

    final double flesch = getFleschIndex(source.getLocale(), source.getComments());
    LOGGER.fine("eval " + MetricName.FLESCH + ": " + flesch);
    result.addResult(MetricName.FLESCH, 1d, flesch);

    final double smog = getSMOGIndex(source.getLocale(), source.getComments());
    LOGGER.fine("eval " + MetricName.SMOG + ": " + smog);
    result.addResult(MetricName.SMOG, 1d, smog);

    return result;

  }

  private double getSMOGIndex(Locale locale, List<Comment> comments) {
    double total = 0d;
    for (Comment c : comments) {
      total += getSMOGIndex(locale, c);
    }
    return total / comments.size();
  }

  private double getFleschIndex(Locale locale, List<Comment> comments) {
    double total = 0d;
    for (Comment c : comments) {
      total += getFleschIndex(locale, c);
    }
    return total / comments.size();
  }

  private double getAmdahlIndex(Locale locale, List<Comment> comments) {

    double total = 0d;
    for (Comment c : comments) {
      total += getAmdahlIndex(locale, c);
    }
    return total / comments.size();
  }

  @Override
  public String name() {
    return "CommentReadingEaseCriterion";
  }
}
