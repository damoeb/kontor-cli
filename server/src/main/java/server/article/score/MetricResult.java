package server.article.score;

public class MetricResult {

  private MetricName metricName;

  private double result;

  public double getResult() {
    return result;
  }

  public void setResult(double result) {
    this.result = result;
  }

  public MetricName getMetricName() {
    return metricName;
  }

  public void setMetricName(MetricName metricName) {
    this.metricName = metricName;
  }
}
