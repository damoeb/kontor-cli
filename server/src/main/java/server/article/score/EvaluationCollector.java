package server.article.score;


import server.article.score.criterion.Criterion;
import server.article.score.criterion.Performance;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

public class EvaluationCollector implements Evaluation {

  private static final Logger LOGGER = Logger.getLogger(EvaluationCollector.class.getName());

  private final transient Map<Criterion, Performance> evaluations = new HashMap<Criterion, Performance>(15);

  @Override
  public void addPerformance(final Criterion criterion, final Performance performance) {
    evaluations.put(criterion, performance);
  }

  @Override
  public Double[] toVector() {
    Vector<Double> vector = new Vector<Double>();
    for (Performance performance : evaluations.values()) {
      Double result = performance.getResult();
      if (result == null) {
        LOGGER.fine("No performance for " + performance.getCriterion().name());
      } else {
        vector.add(result);
      }
    }
    return vector.toArray(new Double[vector.size()]);
  }

  @Override
  public Double quality() {
    Double[] vector = toVector();
    if (vector.length == 0) {
      return null;
    } else {
      return VectorUtils.distance(getMaxQuality(vector.length), vector);
    }
  }

  private Double[] getMaxQuality(int length) {
    Double[] v = new Double[length];
    for (int i = 0; i < v.length; i++) {
      v[i] = 1d;
    }
    return v;
  }

  @Override
  public int compareTo(final Evaluation eval) {
    int c = eval.quality().compareTo(quality());
    return c == 0 ? 1 : c;
  }
}
