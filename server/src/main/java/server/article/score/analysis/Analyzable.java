package server.article.score.analysis;

import java.util.List;

public interface Analyzable {

  void addAnalyzer(Analyzer analyzer);

  List<Analyzer> getAnalyzer();

}
