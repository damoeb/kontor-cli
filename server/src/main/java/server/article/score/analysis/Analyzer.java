package server.article.score.analysis;

import server.article.Article;

public interface Analyzer {

  void analyze(Article article);

}
