package server.article.score;


import server.article.score.criterion.Criterion;
import server.article.score.criterion.Performance;

public interface Evaluation extends Comparable<Evaluation> {

  void addPerformance(Criterion criterion, Performance performance);

  Double[] toVector();

  Double quality();

}
