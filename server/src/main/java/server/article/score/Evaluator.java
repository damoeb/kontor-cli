package server.article.score;

import server.article.Article;
import server.article.score.criterion.Criterion;
import server.article.score.criterion.Goal;

public interface Evaluator<EVALUATION extends Evaluation> extends Constraintable {

  EVALUATION evaluate(Article toEvaluate, Goal goal) throws ConstraintViolation;

  void addCriterion(Criterion criterion);

}
