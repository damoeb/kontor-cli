package server.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.article.score.*;
import server.article.score.criterion.Criterion;
import server.article.score.criterion.CriterionComposite;
import server.article.score.criterion.Goal;
import server.article.score.criterion.Performance;
import server.article.score.criterion.complex.*;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

@Service
public class ArticleScorerService extends CriterionComposite<Criterion> implements Evaluator<Evaluation> {

  private static final transient Logger LOGGER = Logger.getLogger(ArticleScorerService.class.getName());


  // -- CRITERIA -- --------------------------------------------------------------------------------------------------
//    private Freshness freshness;
  private final Novelty novelty;
  private final Recognition recognition;
  private final ActivityGeneration activityGeneration;
  private final Eloquence eloquence;
  private final ReaderDiscussion readerDiscussion;
  private final Upcoming upcoming;

  @Autowired
  public ArticleScorerService(Novelty novelty, Recognition recognition, ActivityGeneration activityGeneration, Eloquence eloquence, ReaderDiscussion readerDiscussion, Upcoming upcoming) {
    this.novelty = novelty;
    this.recognition = recognition;
    this.activityGeneration = activityGeneration;
    this.eloquence = eloquence;
    this.readerDiscussion = readerDiscussion;
    this.upcoming = upcoming;
  }


  @PostConstruct
  public void onInit() {
//      addCriterion(freshness);
    addCriterion(novelty);
    addCriterion(recognition);
    addCriterion(activityGeneration);
    addCriterion(eloquence);
    addCriterion(readerDiscussion);
    addCriterion(upcoming);
//    addAnalyzer(keywordAnalyzer);
//    addAnalyzer(categoryClassifier);
//
//    addConstraint(ensureFreshness);
  }

  @Override
  public Evaluation evaluate(final Article article, final Goal goal) throws ConstraintViolation {

    if (article == null) {
      throw new IllegalArgumentException("Article is null");
    }

    for (Constraint constraint : getConstraints()) {
      constraint.validate(article);
    }

    EvaluationCollector evaluationCollector = new EvaluationCollector();
    for (Criterion criterion : getCriteria()) {
      Performance performance = criterion.evaluatePerformance(article, goal);
      LOGGER.fine(String.format("%s %4.4f", criterion, performance.getResult()));
      evaluationCollector.addPerformance(criterion, performance);
    }

//    for (Analyzer analyzer : getAnalyzer()) {
//      analyzer.analyze(toEvaluate);
//    }

    return evaluationCollector;
  }

  @Override
  public void addCriterion(Criterion criterion) {
    super.addCriterion(criterion);
  }

  public double score(Article article) {
    try {
      return evaluate(article, Goal.MODEST_TEXT).quality();
    } catch (ConstraintViolation constraintViolation) {
      throw new RuntimeException(constraintViolation);
    }
  }
}
