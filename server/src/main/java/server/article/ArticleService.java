package server.article;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import server.feed.Feed;
import server.feed.FeedService;
import server.feed.parser.FeedParser;
import server.shared.KeywordService;
import server.shared.ParserService;
import server.user.UserService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ArticleService {

  private static final Logger logger = Logger.getLogger(ArticleService.class.getName());

  private final KeywordService keywordService;
  private final MongoTemplate mongoTemplate;
  private final ArticleRepository articleRepository;
  private final FeedService feedService;
  private final ParserService parserService;

  public ArticleService(final ArticleRepository articleRepository,
                        final FeedService feedService,
                        final KeywordService keywordService,
                        final ParserService parserService,
                        final MongoTemplate mongoTemplate) {
    this.feedService = feedService;
    this.parserService = parserService;
    this.articleRepository = articleRepository;
    this.keywordService = keywordService;
    this.mongoTemplate = mongoTemplate;
  }

  public List<Article> findAll(Optional<String> feedId,
                               Optional<String> tag,
                               Pageable pageable) {
    Query query = new Query()
      .with(pageable);

    feedId.ifPresent(feedIdValue -> query.addCriteria(Criteria.where("feedId").is(feedIdValue)));
    tag.ifPresent(tagValue -> query.addCriteria(Criteria.where("tags").in(tagValue)));

    return mongoTemplate.find(query, Article.class);
  }

  public boolean save(Article article) {
    if (!articleRepository.existsByUrl(article.getUrl())) {
      logger.info(String.format("Saving article %s %s", article.getUrl(), article.getScore()));
      Set<String> keywords = keywordService.extractKeywords(article);
      article.setTags(keywords);
      articleRepository.insert(article);
      return true;
    }
    return false;
  }

  public List<Article> findAllByFeedIdAndCreatedAtBetween(String feedId, LocalDateTime startDate, LocalDateTime endDate) {
    return this.articleRepository.findAllByFeedIdAndCreatedAtBetween(feedId, startDate, endDate);
  }

  public List<Article> findAllFallbackToLive(String url) {
    Feed feed = feedService.findByUrl(url);
    if (feed == null) {
      FeedParser parser = parserService.getParserByName("atom");
      return parserService.parseFeed(parser, url);
    } else {
      return this.findAll(Optional.of(feed.getId()), Optional.empty(), PageRequest.of(0, 20, Sort.by(Sort.Order.desc("createdAt"))));
    }
  }
}
