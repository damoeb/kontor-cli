package server.article;


import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class ArticleController {

  private ArticleService articleService;

  public ArticleController(ArticleService articleService) {
    this.articleService = articleService;
  }

  @GetMapping("/articles")
  public List<Article> articles(@RequestParam(name = "feedId", required = false) Optional<String> feedId,
                                @RequestParam(name = "tag", required = false) Optional<String> tag,
                                @RequestParam(value = "page", required = false) Optional<Integer> page,
                                @RequestParam(value = "size", required = false) Optional<Integer> size) {
    return articleService.findAll(feedId, tag, PageRequest.of(page.orElse(0), size.orElse(20)));
  }

  @GetMapping("/articles/live")
  public List<Article> articles(@RequestParam(name = "url") String url /*,
                                @RequestParam(name = "parser") String parserName*/) {
    return articleService.findAllFallbackToLive(url);
  }

}
