package server.user;

import org.springframework.data.annotation.Id;
import server.feed.Feed;

import java.util.Set;

public class User {
  @Id
  private String id;
  private Set<String> tags;
  private Set<Feed> sources;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public Set<Feed> getSources() {
    return sources;
  }

  public void setSources(Set<Feed> sources) {
    this.sources = sources;
  }
}
