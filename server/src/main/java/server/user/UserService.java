package server.user;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import server.article.Article;
import server.article.ArticleService;
import server.feed.Feed;
import server.feed.FeedService;
import server.inbox.InboxItem;
import server.inbox.InboxService;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
@Validated
public class UserService {

  private Logger logger = Logger.getLogger(UserService.class.getName());

  private final FeedService feedService;
  private final InboxService inboxService;
  private final ArticleService articleService;
  private final UserRepository userRepository;

  public UserService(final FeedService feedService,
                     final ArticleService articleService,
                     final UserRepository userRepository,
                     final InboxService inboxService) {
    this.userRepository = userRepository;
    this.feedService = feedService;
    this.inboxService = inboxService;
    this.articleService = articleService;
  }

  public Optional<User> findById(String id) {
    return userRepository.findById(id);
  }

  public User push(User updatedUser) {
    logger.info("user #" + updatedUser.getId() + " pushes");
    Optional<User> originalUserOpt = userRepository.findById(updatedUser.getId());
    User originalUser = originalUserOpt.orElseGet(User::new);
    Set<Feed> sources = updatedUser.getSources()
      .parallelStream()
      .map(feed -> {
        try {
          Feed persistedFeed = feedService.save(feed);
          persistedFeed.setWhitelisted(feed.isWhitelisted());
          return persistedFeed;
        } catch(Exception e) {
          return null;
        }
      })
      .filter(Objects::nonNull)
      .collect(Collectors.toSet());

    originalUser.setId(updatedUser.getId());
    originalUser.setSources(sources);
    originalUser.setTags(updatedUser.getTags());

    logger.info("saving user #" + originalUser.getId());
    return userRepository.save(originalUser);
  }

  public List<User> findAll() {
    return userRepository.findAll();
  }

  public void notifySubscriber(User subscriber, Article article) {
    logger.info(String.format("Notifying #%s for %s", subscriber.getId(), article.getUrl()));
    InboxItem inboxItem = new InboxItem();

    inboxItem.setArticleId(article.getId());
    inboxItem.setFeedId(article.getFeedId());
    inboxItem.setUserId(subscriber.getId());
    inboxItem.setScore(article.getScore());
    inboxItem.setCreatedAt(article.getPublishedDate());

    inboxService.add(inboxItem);
  }

  public void fillInboxes() {
    userRepository.findAll().forEach(user -> {
      LocalDateTime startDate = LocalDateTime.now().minus(1, ChronoUnit.DAYS);
      LocalDateTime endDate = LocalDateTime.now();
      user.getSources().forEach(feed -> {
        articleService.findAllByFeedIdAndCreatedAtBetween(feed.getId(), startDate, endDate)
          .stream()
          .filter(article -> feed.isWhitelisted() || hasSomeMatchingTags(user, article))
          .forEach(article -> notifySubscriber(user, article));
      });
    });
  }

  private boolean hasSomeMatchingTags(User user, Article article) {
    return user.getTags()
      .stream()
      .filter(usersTag -> article.getTags().stream().anyMatch(tag -> tag.contains(usersTag)))
      .limit(1)
      .count() > 0;
  }
}
