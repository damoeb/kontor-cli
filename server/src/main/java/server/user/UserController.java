package server.user;


import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

  private UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/users/{id}")
  public Optional<User> get(@PathVariable("id") String userId) {
    return userService.findById(userId);
  }

  @GetMapping("/users")
  public List<User> list() {
    return userService.findAll();
  }

  @PostMapping("/users")
  public User push(@RequestBody User user) {
    return userService.push(user);
  }

}
