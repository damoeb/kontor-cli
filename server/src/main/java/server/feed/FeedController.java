package server.feed;


import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class FeedController {

  private FeedService feedService;

  public FeedController(FeedService feedService) {
    this.feedService = feedService;
  }

  @GetMapping("/feeds")
  public List<Feed> feeds(@RequestParam(name = "url", required = false) Optional<String> urlParam) {
    return feedService.findAll(urlParam);
  }

  @PostMapping("/feeds")
  public Feed set(@RequestBody Feed feed) {
    return feedService.save(feed);
  }

}
