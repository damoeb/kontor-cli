package server.feed.parser;

import server.article.Article;
import server.feed.Feed;

import java.util.List;
import java.util.Set;

public interface FeedParser {
  List<Article> parseArticles(String content);

  boolean matches(String url);

  Feed parseFeed(String content);

  String name();
}
