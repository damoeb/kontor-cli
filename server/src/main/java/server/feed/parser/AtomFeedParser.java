package server.feed.parser;

import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;
import server.article.Article;
import server.feed.Feed;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

@Component
public class AtomFeedParser implements FeedParser {
  @Override
  public List<Article> parseArticles(String content) {
    try {
      SyndFeed syndFeed = getSyndFeed(new ByteArrayInputStream(content.getBytes()));
      return syndFeed.getEntries().stream().map(this::toArticle).collect(Collectors.toList());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private SyndFeed getSyndFeed(InputStream stream) throws Exception {
    SyndFeedInput syndFeedInput = new SyndFeedInput();
    // https://stackoverflow.com/questions/11315439/ignore-dtd-specification-in-scala
    syndFeedInput.setAllowDoctypes(true);
    syndFeedInput.setXmlHealerOn(true);
    return syndFeedInput.build(new XmlReader(stream));
  }

  private String html2text(String html) {
    return Jsoup.parse(html).text();
  }

  private Article toArticle(SyndEntry syndEntry) {
    Article article = new Article();
    article.setAuthor(syndEntry.getAuthor());
    article.setExcerpt(syndEntry.getDescription() == null ? null : html2text(trimToEmpty(syndEntry.getDescription().getValue())));
    article.setContent(syndEntry.getContents()
      .stream()
      .map(SyndContent::getValue)
      .collect(Collectors.joining(". ")));
    article.setTextContent(html2text(article.getContent()));
    if (isBlank(article.getExcerpt())) {
      // todo create excerpt
      article.setExcerpt(article.getTextContent());
    }
    article.setTitle(html2text(syndEntry.getTitle()));
    article.setUrl(syndEntry.getLink());
    article.setDomain(getDomain(syndEntry.getLink()));
    article.setPublishedDate(syndEntry.getPublishedDate());
    article.setCreatedAt(new Date());
    article.setTags(syndEntry.getCategories()
      .stream()
      .map(SyndCategory::getName)
      .collect(Collectors.toSet()));
    article.setEnclosures(parseEnclosures(syndEntry.getEnclosures()));
    return article;
  }

  private Set<Pair<String, String>> parseEnclosures(List<SyndEnclosure> enclosures) {
    return enclosures
      .stream()
      .map(enclosure -> Pair.of(enclosure.getType(), enclosure.getUrl()))
      .collect(Collectors.toSet());
  }

  private String getDomain(String url) {
    try {
      return new URL(url).getHost();
    } catch (MalformedURLException e) {
      return null;
    }
  }

  @Override
  public boolean matches(String url) {
    return false;
  }

  @Override
  public Feed parseFeed(String content) {
    try {
      SyndFeed syndFeed = getSyndFeed(new ByteArrayInputStream(content.getBytes()));
      Feed feed = new Feed();
      feed.setName(syndFeed.getTitle());
//      feed.setSiteLink(syndFeed.getLink());
      feed.setDescription(syndFeed.getDescription());

      return feed;

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String name() {
    return "atom";
  }
}
