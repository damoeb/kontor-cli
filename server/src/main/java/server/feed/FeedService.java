package server.feed;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import server.feed.parser.FeedParser;
import server.shared.DownloadService;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class FeedService {

  private final Logger logger = Logger.getLogger(FeedService.class.getName());
  private final List<FeedParser> feedParsers;
  private final FeedRepository feedRepository;
  private final DownloadService downloadService;
  private final MongoTemplate mongoTemplate;

  public FeedService(final FeedRepository feedRepository,
                     final List<FeedParser> feedParsers,
                     final DownloadService downloadService,
                     final MongoTemplate mongoTemplate) {
    this.feedRepository = feedRepository;
    this.downloadService = downloadService;
    this.mongoTemplate = mongoTemplate;
    this.feedParsers = feedParsers; // todo should be ordered
  }

  public List<Feed> findAll(Optional<String> optionalUrl) {
    Criteria criteria = new Criteria();
    if (optionalUrl.isPresent()) {
      criteria = criteria.and("url").is(optionalUrl.get());
    }
    return mongoTemplate.find(Query.query(criteria), Feed.class);
  }

  private FeedParser findMatchingParser(String url, String content) {
    return feedParsers
      .stream()
//      .filter(feedParser -> feedParser.matches(url))
      .findFirst()
      .get();
  }

  public Feed save(Feed rawFeed) {
    String url = rawFeed.getUrl();

    if (feedRepository.existsByUrl(url)) {
      logger.info("omit saving " + rawFeed.getUrl());
      return feedRepository.findByUrl(url);
    } else {
      logger.info("save " + rawFeed.getUrl());
      try {
        String content = downloadService.fetchUrl(url);
        FeedParser matchingParser = findMatchingParser(url, content);
        Feed feed = matchingParser.parseFeed(content);
        feed.setUrl(url);
        feed.setParser(matchingParser.name());

        logger.info("saving " + feed);
        return feedRepository.insert(feed);
      } catch (Exception e) {
        logger.log(Level.WARNING, "Failed parsing feed for " + rawFeed.getUrl() + ": " + e.getMessage());
        throw new RuntimeException(e);
      }
    }
  }

  public Feed findByUrl(String url) {
    return feedRepository.findByUrl(url);
  }
}
