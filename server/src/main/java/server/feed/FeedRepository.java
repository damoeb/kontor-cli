package server.feed;


import org.springframework.data.mongodb.repository.MongoRepository;

public interface FeedRepository extends MongoRepository<Feed, String> {

  boolean existsByUrl(String url);

  Feed findByUrl(String url);
}
