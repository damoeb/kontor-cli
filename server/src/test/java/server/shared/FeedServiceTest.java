package server.shared;

import com.rometools.rome.io.FeedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import server.article.ArticleRepository;
import server.article.ArticleService;
import server.feed.FeedRepository;
import server.feed.FeedService;

import java.io.IOException;
import java.io.StringReader;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FeedService.class, ArticleService.class})
@EnableConfigurationProperties
public class FeedServiceTest {

  @Autowired
  FeedService feedService;

  @MockBean
  FeedRepository feedRepository;

  @MockBean
  ArticleRepository articleRepository;

  @Test
  public void parseFeed() throws IOException, FeedException {
    int actual = feedService.parseRssFeed(new StringReader("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
      "\n" +
      "<feed xmlns=\"http://www.w3.org/2005/Atom\">\n" +
      "\n" +
      "\t<title>Example Feed</title>\n" +
      "\t<subtitle>A subtitle.</subtitle>\n" +
      "\t<link href=\"http://example.org/feed/\" rel=\"self\" />\n" +
      "\t<link href=\"http://example.org/\" />\n" +
      "\t<id>urn:uuid:60a76c80-d399-11d9-b91C-0003939e0af6</id>\n" +
      "\t<updated>2003-12-13T18:30:02Z</updated>\n" +
      "\t\n" +
      "\t\n" +
      "\t<entry>\n" +
      "\t\t<title>Atom-Powered Robots Run Amok</title>\n" +
      "\t\t<link href=\"http://example.org/2003/12/13/atom03\" />\n" +
      "\t\t<link rel=\"alternate\" type=\"text/html\" href=\"http://example.org/2003/12/13/atom03.html\"/>\n" +
      "\t\t<link rel=\"edit\" href=\"http://example.org/2003/12/13/atom03/edit\"/>\n" +
      "\t\t<id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>\n" +
      "\t\t<updated>2003-12-13T18:30:02Z</updated>\n" +
      "\t\t<summary>Some text.</summary>\n" +
      "\t\t<content type=\"xhtml\">\n" +
      "\t\t\t<div xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "\t\t\t\t<p>This is the entry content.</p>\n" +
      "\t\t\t</div>\n" +
      "\t\t</content>\n" +
      "\t\t<author>\n" +
      "\t\t\t<name>John Doe</name>\n" +
      "\t\t\t<email>johndoe@example.com</email>\n" +
      "\t\t</author>\n" +
      "\t</entry>\n" +
      "\n" +
      "</feed>")).collect(Collectors.toSet()).size();
    assertEquals(1, actual);
  }
}
