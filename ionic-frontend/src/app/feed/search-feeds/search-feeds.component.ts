import { Component, OnInit } from '@angular/core';
import {Feed} from "../../core/feed";
import {AppStateService} from "../../core/app-state.service";
import {SiteService} from "../../core/site.service";
import {Router} from "@angular/router";
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-search-feeds',
  templateUrl: './search-feeds.component.html',
  styleUrls: ['./search-feeds.component.scss'],
})
export class SearchFeedsComponent implements OnInit {

  public filterQuery:string;
  public inSiteFeeds: Array<Feed>;
  private newFeed: Feed;
  public busy: boolean;

  constructor(private appStateService:AppStateService,
              private siteService: SiteService,
              private router:Router,
              private modalController: ModalController) { }

  ngOnInit() {}

  async subscribe(feed:Feed) {
    this.appStateService.subscribeFeed(feed);
  }

  search(filterQuery:string) {
    this.inSiteFeeds = [];
    this.busy = true;
    if (this.isURL(filterQuery)) {
      this.siteService.analyzeSite(filterQuery).toPromise()
        .then(siteAnalysis => {
          this.inSiteFeeds = siteAnalysis.feeds;
        })
        .finally(() => {
          this.busy = false;
        });
    }
  }

  private isURL(str) {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return pattern.test(str);
  }

  showFeedDetails(feed: Feed) {
    this.modalController.dismiss();
    this.router.navigate(['/feeds/new'], {queryParams: {feed:JSON.stringify(feed)}})
  }
}
