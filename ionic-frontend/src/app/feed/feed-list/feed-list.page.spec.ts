import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FeedListPage} from './feed-list.page';

describe('FeedListPage', () => {
  let component: FeedListPage;
  let fixture: ComponentFixture<FeedListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeedListPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
