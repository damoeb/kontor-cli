import {Component, OnInit} from '@angular/core';
import {Feed} from "../../core/feed";
import {AppStateService} from "../../core/app-state.service";
import {ModalController} from "@ionic/angular";
import {SearchFeedsComponent} from "../search-feeds/search-feeds.component";

@Component({
  selector: 'app-feed-list',
  templateUrl: './feed-list.page.html',
  styleUrls: ['./feed-list.page.scss'],
})
export class FeedListPage implements OnInit {

  public feeds: Array<Feed>;
  public filterQuery: string;

  constructor(private appStateService: AppStateService,
              private modalController:ModalController) {
  }

  ngOnInit() {
    this.appStateService.getSources().subscribe(subscriptions => {
      this.feeds = subscriptions
        .filter(feed => feed)
        .sort((feedA, feedB) => {
          if (feedA.name < feedB.name) {
            return -1;
          }
          if (feedA.name > feedB.name) {
            return 1;
          }
          return 0;
        });
    });
  }

  getFilteredFeeds() {
    const query = (this.filterQuery || '').toLowerCase();
    return this.feeds
      .filter(feed => !this.filterQuery
        || (feed.name || '').toLowerCase().indexOf(query) > -1
        || (feed.url || '').toLowerCase().indexOf(query) > -1
        || (feed.description || '').toLowerCase().indexOf(query) > -1)
  }

  async presentAddFeedModal() {
    const modal = await this.modalController.create({
      component: SearchFeedsComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
}
