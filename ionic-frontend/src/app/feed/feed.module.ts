import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {FeedListPage} from './feed-list/feed-list.page';
import {FeedDetailsPage} from "./feed-details/feed-details.page";
import {ArticleModule} from "../article/article.module";
import {SearchFeedsComponent} from "./search-feeds/search-feeds.component";

const routes: Routes = [
  {
    path: '',
    component: FeedListPage
  },
  {
    path: ':feedId',
    component: FeedDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArticleModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [SearchFeedsComponent],
  declarations: [FeedListPage, FeedDetailsPage, SearchFeedsComponent]
})
export class FeedPageModule {
}
