import {Injectable} from '@angular/core';
import {Feed} from "../core/feed";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FeedService {

  constructor(private httpClient: HttpClient) {
  }

  findById(feedId: string): Observable<Feed> {
    return this.httpClient
      .get<Feed>(`/api/feeds/${feedId}`)
      .pipe(map(data => data));
  }
}
