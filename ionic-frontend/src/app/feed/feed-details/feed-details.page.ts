import {Component, OnInit} from '@angular/core';
import {Feed} from "../../core/feed";
import {ActivatedRoute, Router} from "@angular/router";
import {FeedService} from "../feed.service";
import {Article} from "../../core/article";
import {AppStateService} from "../../core/app-state.service";
import {ArticleService} from "../../core/article.service";
import {ActionSheetController, NavController} from "@ionic/angular";

@Component({
  selector: 'app-feed-details',
  templateUrl: './feed-details.page.html',
  styleUrls: ['./feed-details.page.scss'],
})
export class FeedDetailsPage implements OnInit {

  public feed: Feed = null;
  public isSubscribed: boolean;
  public articles: Array<Article>;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private feedService: FeedService,
              private appStateService: AppStateService,
              private articleService: ArticleService,
              private navController: NavController,
              private actionSheetController: ActionSheetController) {
  }

  ngOnInit() {

    const feedId = this.activatedRoute.snapshot.paramMap.get('feedId');
    let subscriptionFilter;

    let isValidId = feedId !== 'new';

    if (isValidId) {

      this.articleService
        .filterByFeedId(feedId)
        .subscribe(articles => {
          this.articles = articles;
        });
      // find by id
      subscriptionFilter = feed => feed.id === feedId;
    } else {

      this.feed = JSON.parse(this.activatedRoute.snapshot.queryParams.feed);

      this.articleService.getLiveArticlesFromFeed(this.feed.url)
        .subscribe(articles => {
          this.articles = articles;
        });
      // find by url
      subscriptionFilter = feed => feed.url === this.feed.url;
    }

    this.appStateService
      .getSources()
      .subscribe(feeds => {
        const subscription = feeds.find(subscriptionFilter);
        if (subscription) {
          this.isSubscribed = true;
          this.feed = subscription;
        } else {
          this.isSubscribed = false;

          if (isValidId) {
            this.feedService
              .findById(feedId)
              .subscribe(feed => {
                this.feed = feed;
              });
          }
        }
      });
  }

  openSubscriptionDetailsWithPush() {
    this.openSubscriptionDetails().then(() => {
      this.appStateService.updateFeed(this.feed);
    });
  }

  openSubscriptionDetails():Promise<void> {
    return new Promise(async (resolve, reject) => {
      const actionSheet = await this.actionSheetController.create({
        header: 'Subscription Details',
        buttons: [
          {
            text: 'All Articles',
            icon: 'heart',
            handler: () => {
              this.feed.whitelisted = true;
              resolve();
            }
          },
          {
            text: 'Matching Tags',
            icon: 'heart-half',
            handler: () => {
              this.feed.whitelisted = false;
              resolve();
            }
          },
          {
            text: 'Cancel',
            icon: 'close',
            handler: () => {
              reject();
            }
          }
        ]
      });
      await actionSheet.present();
    });
  }

  async subscribe(doSubscribe: boolean) {
    if (doSubscribe) {
      this.openSubscriptionDetails().then(() => {
        this.appStateService.subscribeFeed(this.feed);
        this.navController.navigateForward('/feeds');
      });
    } else {
      this.appStateService.unsubscribeFeed(this.feed);
      this.navController.navigateForward('/feeds');
    }
  }
}
