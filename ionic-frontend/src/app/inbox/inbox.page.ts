import {Component, OnInit} from '@angular/core';
import {Article} from "../core/article";
import {InboxService} from "../core/inbox.service";

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage implements OnInit {

  public articles: Array<Article> = [];

  constructor(private inboxService: InboxService) {
  }

  ngOnInit() {
    this.inboxService.getInbox().then(articles => {
      this.articles = articles;
    });
  }
}
