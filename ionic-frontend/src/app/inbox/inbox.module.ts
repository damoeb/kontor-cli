import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {InboxPage} from './inbox.page';
import {ArticleModule} from "../article/article.module";

const routes: Routes = [
  {
    path: '',
    component: InboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArticleModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InboxPage]
})
export class InboxPageModule {
}
