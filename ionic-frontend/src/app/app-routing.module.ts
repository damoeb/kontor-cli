import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inbox',
    pathMatch: 'full'
  },
  {
    path: 'feeds',
    loadChildren: './feed/feed.module#FeedPageModule'
  },
  {
    path: 'inbox',
    loadChildren: './inbox/inbox.module#InboxPageModule'
  },
  {
    path: 'tags',
    loadChildren: './tag/tag.module#TagPageModule'
  },
  {
    path: 'articles',
    loadChildren: './article/article.module#ArticlePageModule'
  },
  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsPageModule'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
