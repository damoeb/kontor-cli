import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ArticleListComponent} from "./article-list/article-list.component";
import {RouterModule, Routes} from "@angular/router";
import {IonicModule} from "@ionic/angular";
import {ArticleDetailsPage} from "./article-details/article-details.page";
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: ':articleId',
    component: ArticleDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule
  ],
  declarations: [ArticleListComponent],
  exports: [ArticleListComponent]
})
export class ArticleModule {
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArticleDetailsPage]
})
export class ArticlePageModule {
}
