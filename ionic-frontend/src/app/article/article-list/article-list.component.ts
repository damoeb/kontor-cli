import * as dayjs from 'dayjs';
import * as relativeTime from 'dayjs/plugin/relativeTime'

import {Component, Input, OnInit} from '@angular/core';
import {Article} from "../../core/article";
import {AppStateService} from "../../core/app-state.service";

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
  inputs: ['articles']
})
export class ArticleListComponent implements OnInit {

  @Input('articles')
  public articles: Array<Article>;

  constructor(private appStateService:AppStateService) {
    dayjs.extend(relativeTime)
  }

  ngOnInit() {
  }

  getFaviconUrl(article: Article) {
    return `/api/site/favicon?url=${article.url}`
  }

  getArticleDetailsUrl(article: Article) {
    return `/articles/${article.id}`;
  }

  timeAgo(publishedDate: string) {
    return dayjs(publishedDate).fromNow()
  }

  matchingTags(tags: Array<string>) {
    return this.appStateService.getMatchingTags(tags);
  }
}
