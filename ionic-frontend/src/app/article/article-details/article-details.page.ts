import {Component, OnInit} from '@angular/core';
import {ArticleService} from "../../core/article.service";
import {ActivatedRoute} from "@angular/router";
import {Article} from "../../core/article";
import {FeedService} from "../../feed/feed.service";
import {Feed} from "../../core/feed";
import {AppStateService} from "../../core/app-state.service";

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.page.html',
  styleUrls: ['./article-details.page.scss'],
})
export class ArticleDetailsPage implements OnInit {
  public article: Article;
  public feed: Feed;

  constructor(private articleService: ArticleService,
              private appStateService: AppStateService,
              private feedService: FeedService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(observer => {
      const articleId = observer.get('articleId');
      this.articleService.findById(articleId).subscribe(article => {
        this.article = article;
        this.feedService.findById(article.feedId).toPromise().then(feed => {
          this.feed = feed;
        })
      });
    });
  }

  readText() {
    // todo use androids tts
  }

  matchingTags(tags: Array<string>) {
    return this.appStateService.getMatchingTags(tags);
  }
}
