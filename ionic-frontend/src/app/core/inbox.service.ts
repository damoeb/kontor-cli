import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AppStateService} from "./app-state.service";
import {Article} from "./article";
import {ArticleService} from "./article.service";

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor(private httpClient: HttpClient,
              private appStateService: AppStateService,
              private articleService: ArticleService) {
  }

  getInbox(): Promise<Array<Article>> {
    return this.httpClient
      .get<any>(`/api/inbox/${this.appStateService.currentUserId()}`).toPromise().then(data => {
        return Promise.all((<Array<any>>data.content).map((item) => this.articleService.findById(item.articleId).toPromise().then(article => {
          article.id = item.articleId;
          return article;
        })))
      });
  }
}
