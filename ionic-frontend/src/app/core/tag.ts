export interface Tag {
  name: string
  highlighted?: boolean
}
