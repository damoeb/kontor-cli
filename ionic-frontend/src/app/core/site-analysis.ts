import {Feed} from "./feed";

export interface SiteAnalysis {
  title: string;
  feeds: Array<Feed>
}
