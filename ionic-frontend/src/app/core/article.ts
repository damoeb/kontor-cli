export interface Article {
  id: string
  url: string
  title: string
  domain: string
  excerpt: string
  content: string
  textContent: string
  feedId: string
  score: number
  tags: Array<string>
  publishedDate: string
  createdAt: string
  lastModifiedAt: string
  locale: string
  comments: any
}
