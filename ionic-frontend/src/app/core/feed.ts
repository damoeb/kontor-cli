export interface Feed {
  id: string
  name: string
  url: string
  description: string
  lastModifiedAt: Date
  parser?: string
  whitelisted: boolean
}
