import {Injectable} from '@angular/core';
import {Feed} from "./feed";
import {BehaviorSubject, Observable, Observer, of} from "rxjs";
import {User} from "./user";
import {HttpClient} from "@angular/common/http";
import {ToastController} from "@ionic/angular";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AppStateService {

  private currentUser:User;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private httpClient: HttpClient,
              private toastController: ToastController) {
  }

  getSources(): Observable<Array<Feed>> {
    return this.currentUserSubject.pipe(map(user => user.sources));
  }

  getTags(): Observable<Array<string>> {
    return this.currentUserSubject.pipe(map(user => user.tags));
  }

  subscribeTag(tagName: string) {
    if (this.currentUser.tags.indexOf(tagName) === -1) {
      this.currentUser.tags.push(tagName);
    }
    this.presentToast(`Subscribed #${tagName}`);
    this.fireUpdate();
  }

  unsubscribeTag(tagName: string) {
    this.currentUser.tags = this.currentUser.tags.filter(otherTag => otherTag !== tagName);
    this.presentToast(`Unsubscribed from #${tagName}`);
    this.fireUpdate();
  }

  unsubscribeFeed(feed: Feed) {
    this.currentUser.sources = this.currentUser.sources.filter(otherFeed => otherFeed.url !== feed.url);
    this.fireUpdate();
    this.presentToast(`Unsubscribed from ${feed.name}`);

    this.push();
  }

  subscribeFeed(feed: Feed) {
    this.currentUser.sources.push(feed);
    this.fireUpdate();
    this.presentToast(`Subscribed to ${feed.name}`);

    this.push();
  }

  private fireUpdate() {
    this.currentUserSubject.next(this.currentUser);
  }

  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  currentUserId(): string {
    return '5ca51e06a5fb2d1ab3dc2c28';
  }

  private pullById(userId: string): Observable<User> {
    return this.httpClient
      .get<User>(`/api/users/${userId}`)
  }

  private push() {
    this.httpClient
      .post<User>(`/api/users`, this.currentUser).toPromise().then(user => {
      this.currentUser = user;
      this.fireUpdate();
    });
  }

  initApp():Promise<void> {
    return this.pullById(this.currentUserId()).toPromise().then(user => {
      this.currentUser = user;
      this.currentUserSubject = new BehaviorSubject<User>(this.currentUser);
    });
  }

  updateFeed(feed: Feed) {
    // remove
    this.currentUser.sources = this.currentUser.sources.filter((otherFeed:Feed) => {
      return otherFeed.url != feed.url;
    });
    this.currentUser.sources.push(feed);
    this.push();
  }

  getMatchingTags(tags: Array<string>) {
    // todo filter tags the user uses
    return tags;
  }

  getUser():Observable<User> {
    return this.currentUserSubject;
  }
}
