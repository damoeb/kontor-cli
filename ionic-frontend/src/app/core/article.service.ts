import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Article} from "./article";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private httpClient: HttpClient) {
  }

  filterByTag(tag: string): Observable<Array<Article>> {
    return this.httpClient
      .get<Array<Article>>(`/api/articles/?tag=${tag}`);
  }

  filterByFeedId(feedId: string): Observable<Array<Article>> {
    return this.httpClient
      .get<Array<Article>>(`/api/articles/?feedId=${feedId}`);
  }

  findById(articleId: string): Observable<Article> {
    return this.httpClient
      .get<Article>(`/api/articles/${articleId}`);
  }

  getLiveArticlesFromFeed(url: string):Observable<Array<Article>> {
    return this.httpClient
      .get<Array<Article>>(`/api/articles/live/?url=${url}`);

  }
}
