import {Feed} from "./feed";
import {InboxSettings} from "./inbox-settings";
import {RepositorySettings} from "./repository-settings";

export interface User {
  id: string
  repository: RepositorySettings
  inbox: InboxSettings
  tags: Array<string>
  sources: Array<Feed>
}
