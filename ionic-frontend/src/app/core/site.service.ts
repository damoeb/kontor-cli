import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Article} from "./article";
import {SiteAnalysis} from "./site-analysis";

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private httpClient: HttpClient) {
  }

  analyzeSite(url: string): Observable<SiteAnalysis> {
    return this.httpClient
      .get<SiteAnalysis>(`/api/site/analysis/?url=${encodeURIComponent(url)}`);
  }
}
