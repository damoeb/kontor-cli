import {Component, OnInit} from '@angular/core';
import {AppStateService} from "../../core/app-state.service";
import {ModalController} from "@ionic/angular";
import {AddTagComponent} from "../add-tag/add-tag.component";

@Component({
  selector: 'app-tags',
  templateUrl: './tag-list.page.html',
  styleUrls: ['./tag-list.page.scss'],
})
export class TagListPage implements OnInit {

  public tags: Array<string>;
  public filterQuery: string;

  constructor(private userService: AppStateService,
              private modalController:ModalController) {
  }

  ngOnInit() {
    this.userService.getTags().subscribe(tags => {
      this.tags = tags;
    });
  }

  getFilteredTags() {
    const query = (this.filterQuery || '').toLowerCase();
    return this.tags
      .filter(tag => !this.filterQuery
        || (tag || '').toLowerCase().indexOf(query) > -1);
  }

  async presentAddTagModal() {
    const modal = await this.modalController.create({
      component: AddTagComponent
    });
    return await modal.present();
  }
}
