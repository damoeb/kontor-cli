import {Component, OnInit} from '@angular/core';
import {AppStateService} from "../../core/app-state.service";
import {ActivatedRoute} from "@angular/router";
import {ArticleService} from "../../core/article.service";
import {Article} from "../../core/article";
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-tag-details',
  templateUrl: './tag-details.page.html',
  styleUrls: ['./tag-details.page.scss'],
})
export class TagDetailsPage implements OnInit {
  public isSubscribed: boolean;
  public tagName: string;
  public articles: Array<Article>;

  constructor(private userService: AppStateService,
              private articleService: ArticleService,
              private route: ActivatedRoute,
              private navController: NavController) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(observer => {
      this.tagName = observer.get('tagName');
    });

    this.articleService
      .filterByTag(this.tagName)
      .subscribe(articles => {
        this.articles = articles;
      });

    this.userService.getTags().subscribe(subscribedTags => {
      this.isSubscribed = subscribedTags.some(otherTag => otherTag === this.tagName)
    });
  }

  async subscribe(doSubscribe: boolean) {
    if (doSubscribe) {
      this.userService.subscribeTag(this.tagName);
    } else {
      this.userService.unsubscribeTag(this.tagName);
    }
    this.navController.navigateForward('/tags')
  }
}
