import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TagDetailsPage} from './tag-details.page';

describe('TagDetailsPage', () => {
  let component: TagDetailsPage;
  let fixture: ComponentFixture<TagDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagDetailsPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
