import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {TagListPage} from './tag-list/tag-list.page';
import {TagDetailsPage} from "./tag-details/tag-details.page";
import {ArticleModule} from "../article/article.module";
import {AddTagComponent} from "./add-tag/add-tag.component";

const routes: Routes = [
  {
    path: '',
    component: TagListPage
  },
  {
    path: ':tagName',
    component: TagDetailsPage
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArticleModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [AddTagComponent],
  declarations: [TagListPage, TagDetailsPage, AddTagComponent]
})
export class TagPageModule {
}
