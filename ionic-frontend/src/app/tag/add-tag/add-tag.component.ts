import { Component, OnInit } from '@angular/core';
import {AppStateService} from "../../core/app-state.service";
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss'],
})
export class AddTagComponent implements OnInit {
  tagName: string;

  constructor(private userService:AppStateService,
              private modalController: ModalController) { }

  ngOnInit() {}

  saveTag() {
    this.userService.subscribeTag(this.tagName);
    this.dismissModal();
  }

  dismissModal() {
    return this.modalController.dismiss();
  }
}
