import {Component, OnInit} from '@angular/core';
import {AppStateService} from "../core/app-state.service";
import {RepositorySettings} from "../core/repository-settings";
import {InboxSettings} from "../core/inbox-settings";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  public repositorySettings: RepositorySettings;
  public inboxSettings: InboxSettings;

  constructor(private appStateService: AppStateService) {
  }

  ngOnInit() {
    this.appStateService.getUser().subscribe(user => {
      this.repositorySettings = user.repository;
      this.inboxSettings = user.inbox;
    });
  }

}
