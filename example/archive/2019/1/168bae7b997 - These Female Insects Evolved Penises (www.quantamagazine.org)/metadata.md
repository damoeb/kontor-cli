---
title: "These Female Insects Evolved Penises"
date: 2019-02-04T23:46:15.063Z
draft: true
byline: "By Jordana CepelewiczJanuary 30, 2019"
length: 10280
excerpt: "Among these cave insects, the females evolved to have penises — twice. The reasons challenge common assumptions about sex."
via:
  - panter
authors:
  - damoeb
tags:
  - insects
  - biology
url: "https://www.quantamagazine.org/why-evolution-reversed-these-insects-sex-organs-20190130"
attachments: 
  - ./readability.html
---
